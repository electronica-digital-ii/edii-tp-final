
bankS	macro	x
	if x == 0
		bcf	STATUS,RP0
		bcf	STATUS,RP1
	endif
	if x == 1
		bsf	STATUS,RP0
		bcf	STATUS,RP1
	endif
	if x == 2
		bcf	STATUS,RP0
		bsf	STATUS,RP1
	endif
	if x == 3
		bsf	STATUS,RP0
		bsf	STATUS,RP1
	endif
	endm

mov	macro	register, literal
	movlw	literal
	movwf	register
	endm
	
movr	macro	register1,register2
	movf	register2,0
	movwf	register1
	endm

