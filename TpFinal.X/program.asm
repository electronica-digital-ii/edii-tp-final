    LIST    p=16f887
    INCLUDE	<p16f887.inc>
    INCLUDE	<macros.inc>

    ;Asignacion de Etiquetas
    ;===========================
    cblock	0x20
    STAT        ;0x20
    ST1         ;0x21
    STI1        ;0x22
    ST3         ;0x23
    ST4         ;0x24
    ST5         ;0x25
    ST6         ;0x26
    STI2    	;0x27
    V1CrRojDec	;0x28
    V1CrRojUni	;0x29
    V1CrVerdDec	;0x2A
    V1CrVerdUni	;0x2B
    V1GiRojDec	;0x2C
    V1GiRojUni	;0x2D
    V1GiVerdDec	;0x2E
    V1GiVerdUni	;0x2F
    V2CrRojDec	;0x30
    V2CrRojUni	;0x31
    V2CrVerdDec	;0x32
    V2CrVerdUni	;0x33
    V3CrRojDec	;0x34
    V3CrRojUni	;0x35
    V3CrVerdDec	;0x36
    V3CrVerdUni	;0x37
    C_TURN      ;0x38   
    INT         ;0x39
    
    LONG_TIME
    TIME    
    COUNTER_SELECT
    RETARD
    RETARD_1
    
    INIT_MSJ
    ID_PARAMETER
    PARAMETER_NIBBLE_H
    PARAMETER_NIBBLE_L
    END_MSJ
    BYTE_COUNTER  
    
    BYTE_SEND_COUNTER  
    BYTE_SEND       ;0x46
    endc
    
    cblock	0x50    
    P_ST1           ;0x50
    P_STI1          ;0x51
    P_ST3           ;0x52
    P_ST4           ;0x53
    P_ST5           ;0x54
    P_ST6           ;0x55
    P_STI2          ;0x56
    P_C_TURN		;0x57
    P_V1CrRojDec	;0x58
    P_V1CrRojUni	;0x59
    P_V1CrVerdDec	;0x5A
    P_V1CrVerdUni	;0x5B
    P_V1GiRojDec	;0x5C
    P_V1GiRojUni	;0x5D
    P_V1GiVerdDec	;0x5E
    P_V1GiVerdUni	;0x5F
    P_V2CrRojDec	;0x60
    P_V2CrRojUni	;0x61
    P_V2CrVerdDec	;0x62
    P_V2CrVerdUni	;0x63
    P_V3CrRojDec	;0x64
    P_V3CrRojUni	;0x65
    P_V3CrVerdDec	;0x66
    P_V3CrVerdUni	;0x67

    endc
    ;Fin Asignacion de Etiquetas
    ;===========================

    org	    0x00
    goto    INICIALIZACION
    org	    0x04
    goto    INTERRUPCION
    
    ;===========================
    ;	INICIO INICIALIZACION    
INICIALIZACION
    call    I_PUERTOS
    call    I_VALORES_DEFECTO   
    call    I_TIMER0
    call    I_TIMER1
    call    I_PUERTO_SERIE
    call    I_INTERRUPCIONES
    clrwdt                          ;Limpio el  WDT para evitar un reseteo  
    goto    $-1
    ;	  FIN INICIALIZACION
    ;===========================
    
    ;===========================
    ;	INICIO I_VALORES_DEFECTO    
I_VALORES_DEFECTO

    ; Seteo de timpo(segundos) para los distintos estados
    bankS    .0 
    mov	    P_ST1,.20        
    mov	    P_STI1,.1 
    mov	    P_ST3,.30 
    mov	    P_ST4,.5 
    mov	    P_ST5,.30 
    mov	    P_ST6,.5 
    mov	    P_STI2,.2    
    
    ; Inicializacion parametros tiempo de displays
    mov	    P_V1CrRojDec,.3	
    mov	    P_V1CrRojUni,.4
    mov	    P_V1CrVerdDec,.5
    mov	    V1CrVerdDec,.5
    mov	    P_V1CrVerdUni,.8
    mov	    V1CrVerdUni,.8
    mov	    P_V1GiRojDec,.6
    mov	    P_V1GiRojUni,.9
    mov	    P_V1GiVerdDec,.2
    mov	    V1GiVerdDec,.2	
    mov	    P_V1GiVerdUni,.9
    mov	    V1GiVerdUni,.9	
    mov	    P_V2CrRojDec,.6	
    mov	    V2CrRojDec,.2	
    mov	    P_V2CrRojUni,.4	
    mov	    V2CrRojUni,.9
    mov	    P_V2CrVerdDec,.2	
    mov	    P_V2CrVerdUni,.9	
    mov	    P_V3CrRojDec,.6 
    mov	    V3CrRojDec,.6	
    mov	    P_V3CrRojUni,.4
    mov	    V3CrRojUni,.4	
    mov	    P_V3CrVerdDec,.2	
    mov	    P_V3CrVerdUni,.9
       
    mov	    P_C_TURN,.5
    mov	    C_TURN,.5

    ; Se setean las salidas de los puertos D y C conectados a los LEDs
    ; con la configuracion inicial del estado 1

    mov	    STAT,.1     
    call    OUTPUT_PD
    movwf   PORTD    
    movfw   STAT
    call    OUTPUT_PC
    movwf   PORTC

    ;Se setea el tiempo inicial del estado 1  
    movr    TIME,P_ST1       
    mov	    LONG_TIME,.10
    
    clrf    BYTE_COUNTER
    return
    ;	  FIN I_VALORES_DEFECTO
    ;===========================
    
    ;===========================
    ;	INICIO OUTPUT_PD   
OUTPUT_PD    
    addwf   PCL
    retlw   .0   
    retlw	b'00101100'
    retlw	b'00100100'
    retlw	b'00110001'
    retlw	b'01010010'
    retlw	b'10010100'
    retlw	b'10010100'
    retlw	b'01010010'
    retlw	b'00000000'


   
    ;	  FIN OUTPUT_PD
    ;===========================
    
    ;===========================
    ;	INICIO OUTPUT_PC   
OUTPUT_PC   
    addwf   PCL
    retlw   .0
    retlw	b'00001000'
    retlw	b'00001000'
    retlw	b'00001000'
    retlw	b'00001000'
    retlw	b'00000010'
    retlw	b'00000100'
    retlw	b'00001000'
    retlw	b'00000000'


    ;	  FIN OUTPUT_PC
    ;===========================
    
        
    ;===========================
    ;	INICIO I_PUERTOS   
I_PUERTOS    
    banksel	TRISC       ;Configuramos los puertos A,B,C y D como salidas 
    clrf	TRISA
    clrf	TRISB
    clrf	TRISD
    clrf	TRISC
    
    bsf		TRISC,RC7   ;puerto RC7 como entrada para la recepcion de datos
    
    banksel	ANSEL       ;Configuramos los puertos para que sean entradas o salidas digitales
    clrf	ANSEL
    clrf	ANSELH    
    
    bankS	.0 
    
    clrf	PORTA       ;Limpio los puertos A,B,C y D
    clrf	PORTB
    clrf	PORTC
    clrf	PORTD
    
    return
    ;	  FIN I_PUERTOS
    ;===========================
    
    ;===========================
    ;	INICIO I_TIMER0   
I_TIMER0    
    banksel	OPTION_REG
    
    bcf		OPTION_REG,T0CS	    ;Selecciono Clock Interno
    bsf		OPTION_REG,PS2	    ;Seleccionamos Prescale x256
    bsf		OPTION_REG,PS1      
    bsf		OPTION_REG,PS0
    bcf		OPTION_REG,PSA      ;Seleccion del TMR0
I_TIME_TIMER0
    bankS	.0                  ;Para nuestro programa se selecciono un TMR0 de 189
    mov		TMR0,0xBD           ;para obtenemos un retardo de  0.01 segundo  
    return                      
   
    ;	  FIN I_TIMER0
    ;===========================
    
    ;===========================
    ;	INICIO I_TIMER1   
I_TIMER1    
    banksel	T1CON
    
    bcf		T1CON,TMR1CS	    ;Selecciono Clock Interno
    bsf		T1CON,T1CKPS1	    ;Seleccionamos Prescale x8
    bsf		T1CON,T1CKPS0        
I_TIME_TIMER1
    bankS	.0  
    mov		TMR1L,0x2B          ;Para nuestro programa se selecciono un TMR1 de 53035 
    mov		TMR1H,0xCF          ;para obtenemos un retardo de 0.1 segundo  
    bsf		T1CON,TMR1ON        ;habilito el TIMER1
    return
   
    ;	  FIN I_TIMER1
    ;===========================
    
    ;===========================
    ;	INICIO I_PUERTO_SERIE   
I_PUERTO_SERIE    
    
    banksel	SPBRG               ;Se inicializa el puerto serie para una velocidad de   
    mov		SPBRG,.25           ;transferencia de 9600 baudios con una frecuencia de 4MHz   
    mov		TXSTA,b'00100100'    
    banksel	BAUDCTL
    bcf		BAUDCTL,BRG16
    
    banksel	RCSTA
    
    bsf		RCSTA,SPEN          ;Habilito la recepcion y emicion de datos, RC6 y RC7 quedan asignados a EUSAR 
    bsf		RCSTA,CREN          ;Habilito el receptor
    
    return
    ;	  FIN I_PUERTO_SERIE
    ;===========================
    
    ;===========================
    ;	INICIO I_INTERRUPCIONES   
I_INTERRUPCIONES    
    banksel	INTCON    
    bsf		INTCON,PEIE         ; Habilito Interrupciones de perifericos
    bsf		INTCON,T0IE         ; Habilito interrupcion de Timer 0
    
    banksel	PIE1    
    bsf		PIE1,TMR1IE         ; Habilito Interrupciones de Timer 1    
    bsf		PIE1,RCIE
    
    bcf		PIE1,TXIE           ; Deshabilito Interrupcion de EUSAR TRANSMISOR (se habilitara cuando transmitamos) 
    bsf		INTCON,GIE          ; Habilito Interrupciones Globales 
    return
    ;	  FIN I_INTERRUPCIONES
    ;===========================
    
    ;===========================
    ;	INICIO INTERRUPCION   
INTERRUPCION
    bankS   .0
    btfsc	PIR1,TMR1IF         ;Verifica si entro por interrupcion del TIMER1
    call	INT_TIMER1
    btfsc	INTCON,T0IF         ;Verifica si entro por interrupcion del TIMER0
    call	INT_TIMER0   
    btfsc	PIR1,RCIF           ;Verifica si entro por interrupcion de la EUSAR RECEPTOR
    call	INT_RECEPTION_DATA
    ;call	NO_RECEPTION
    btfsc	PIR1,TXIF           ;Verifica si entro por interrupcion de la EUSAR TRANSMISOR
    call	INT_SEND_DATA
    retfie
    ;	  FIN INTERRUPCION
    ;===========================
NO_RECEPTION
    banksel	PORTA
    bsf		PORTA,RA1
    return
    ;===========================
    ;	INICIO INT_TIMER0   
INT_TIMER0
    
    call	SIGNALS_REFRESH
    call	COUNTER_REFRESH  
    
    bcf		INTCON,T0IF
    return
    ;	  FIN INT_TIMER0
    ;===========================
    
    ;===========================
    ;	INICIO SIGNALS_REFRESH       
SIGNALS_REFRESH
    movfw	STAT                ;Tomamos el estado en el que estamos y llamos a la tabla 
    call	OUTPUT_PD           ;para que se actualicen los valores de los LEDs conectados 
    movwf	PORTD               ;al puerto D
    
    movfw	STAT                ;Tomamos el estado en el que estamos y llamos a la tabla 
    call	OUTPUT_PC           ;para que se actualicen los valores de los LEDs conectados 
    movwf	PORTC               ;al puerto C
    ;	  FIN SIGNALS_REFRESH
    ;===========================
    
    ;===========================
    ;	INICIO COUNTER_REFRESH    
COUNTER_REFRESH    
    bsf		PORTA,RA0           ;Coloco un 1 en RA0 para actualizar los Displays
    mov		COUNTER_SELECT,.0   ;Pongo el Selector de los Multiplexores en 0H
CHECK_COUNTER_0    
    movlw	.0
    xorwf	COUNTER_SELECT,W    ;Realiza una XOR entre 0H y COUNTER_SELECT y lo guarda en W
    btfss	STATUS,Z            ;Si Z=0 se realiza la siguiente linea
    goto	CHECK_COUNTER_1    
    swapf	V2CrRojDec,W        ;cambia el NH por el NL de  V2CrRojDec y lo guarda en W
    goto	SHOW_COUNTER
CHECK_COUNTER_1
    movlw	.1
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_2
    swapf	V2CrRojUni,W 
    goto	SHOW_COUNTER
CHECK_COUNTER_2
    movlw	.2
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_3
    swapf	V2CrVerdDec,W 
    goto	SHOW_COUNTER
CHECK_COUNTER_3
    movlw	.3
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_4
    swapf	V2CrVerdUni,W 
    goto	SHOW_COUNTER   
CHECK_COUNTER_4
    movlw	.4
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_5
    swapf	V1CrRojDec,W 
    goto	SHOW_COUNTER  
CHECK_COUNTER_5
    movlw	.5
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_6
    swapf	V1CrRojUni,W 
    goto	SHOW_COUNTER 
CHECK_COUNTER_6
    movlw	.6
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_7
    swapf	V1CrVerdDec,W 
    goto	SHOW_COUNTER
CHECK_COUNTER_7
    movlw	.7
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_8
    swapf	V1CrVerdUni,W 
    goto	SHOW_COUNTER
CHECK_COUNTER_8
    movlw	.8
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_9
    swapf	V1GiRojDec,W 
    goto	SHOW_COUNTER
CHECK_COUNTER_9
    movlw	.9
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_10
    swapf	V1GiRojUni,W 
    goto	SHOW_COUNTER   
CHECK_COUNTER_10
    movlw	.10
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_11
    swapf	V1GiVerdDec,W 
    goto	SHOW_COUNTER   
CHECK_COUNTER_11
    movlw	.11
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_12
    swapf	V1GiVerdUni,W 
    goto	SHOW_COUNTER   
CHECK_COUNTER_12
    movlw	.12
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_13
    swapf	V3CrRojDec,W 
    goto	SHOW_COUNTER    
CHECK_COUNTER_13
    movlw	.13
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_14
    swapf	V3CrRojUni,W 
    goto	SHOW_COUNTER    
CHECK_COUNTER_14
    movlw	.14
    xorwf	COUNTER_SELECT,W
    btfss	STATUS,Z
    goto	CHECK_COUNTER_15
    swapf	V3CrVerdDec,W 
    goto	SHOW_COUNTER
CHECK_COUNTER_15    
    swapf	V3CrVerdUni,W 
    goto	SHOW_COUNTER
SHOW_COUNTER
    addwf	COUNTER_SELECT,W
    bankS   .0
    movwf	PORTB           ;Cargo el puerto B con el bit que setea los Multiplexores(NL) y los Decodificadores(NH)  
    bcf		PORTA,RA0       ;Refresca los Displays
    bsf		PORTA,RA0
    movlw	.1
    addwf	COUNTER_SELECT,F
    btfss	STATUS,DC       ;Si el DC=0 realiza la siguiente linea
    goto	CHECK_COUNTER_0    
    return
    
    ;	  FIN COUNTER_REFRESH
    ;===========================
    
    ;===========================
    ;	INICIO INT_TIMER1   
INT_TIMER1
    
    banksel	T1CON
    bcf		T1CON,TMR1ON        ;Deshabilito del TIMER1 en 0    
    
    bankS	.0
    
    decfsz	LONG_TIME           ;decremento en 1 el LONG_TIME si el resultado es 0 hace un NOP y salta la siguiente instruccion
    goto	FIN_INT_TIMMER
    call	COUNTER_CALC_V1_Roj
    call	COUNTER_CALC_V1_Ver
    call	COUNTER_CALC_V1_Gi_Roj
    call	COUNTER_CALC_V1_Gi_Ver
    call	COUNTER_CALC_V2_Roj
    call	COUNTER_CALC_V2_Ver
    call	COUNTER_CALC_V3_Roj
    call	COUNTER_CALC_V3_Ver
    decfsz	TIME                ;decremento en 1 el TIME si el resultado es 0 hace un NOP y salta la siguiente instruccion
    goto	FIN_INT_SET_LONG_TIME
    
       
TESTST1                         ;Verifica si estamos en el Estado 1 y si puedo pasar al estado siguiente 
    movlw	.1
    xorwf	STAT,W
    btfss	STATUS,Z
    goto	TESTST2
    call	STATUS1
    goto	FIN_INT_SET_STAT
    
TESTST2                         ;Verifica si estamos en el Estado 2 y si puedo pasar al estado siguiente
    movlw	.2
    xorwf	STAT,W
    btfss	STATUS,Z
    goto	TESTST3
    call	STATUS2
    goto	FIN_INT_SET_STAT
    
TESTST3                         ;Verifica si estamos en el Estado 3 y si puedo pasar al estado siguiente
    movlw	.3
    xorwf	STAT,W
    btfss	STATUS,Z
    goto	TESTST4
    call	STATUS3
    goto	FIN_INT_SET_STAT
    
TESTST4                         ;Verifica si estamos en el Estado 4 y si puedo pasar al estado siguiente
    movlw	.4
    xorwf	STAT,W
    btfss	STATUS,Z
    goto	TESTST5
    call	STATUS4
    goto	FIN_INT_SET_STAT
    
TESTST5                         ;Verifica si estamos en el Estado 5 y si puedo pasar al estado siguiente
    movlw	.5
    xorwf	STAT,W
    btfss	STATUS,Z
    goto	TESTST6
    call	STATUS5
    goto	FIN_INT_SET_STAT
    
TESTST6                         ;Verifica si estamos en el Estado 6 y si puedo pasar al estado siguiente
    movlw	.6
    xorwf	STAT,W
    btfss	STATUS,Z
    goto	TESTST7
    call	STATUS6
    goto	FIN_INT_SET_STAT
    
TESTST7                         ;Verifica si estamos en el Estado 7 y si puedo pasar al estado siguiente
    movlw	.7
    xorwf	STAT,W
    btfss	STATUS,Z
    goto	TESTST8
    call	STATUS7
    goto	FIN_INT_SET_STAT
    
TESTST8                         ;Verifica si estamos en el Estado 8 y si puedo pasar al estado siguiente
    movlw	.8
    xorwf	STAT,W
    btfss	STATUS,Z
    goto	FIN_INT_SET_STAT
    call	STATUS8
    
FIN_INT_SET_STAT    
    movwf	STAT     
FIN_INT_SET_LONG_TIME    
    mov		LONG_TIME,.10       ;Cargo el LONG_TIME con 10 por que necesito un retardo de 1 segundo     
    call	INIT_INT_SEND_DATA   
FIN_INT_TIMMER       
    call	I_TIME_TIMER1      
    banksel	PIR1
    bcf		PIR1,TMR1IF         ;bajo la bandera del TIMER1   
    return
        
    
    ;	  FIN INT_TIMER1
    ;===========================
    
    ;===========================
    ;	INICIO COUNTER_CALC_V1_Roj   
COUNTER_CALC_V1_Roj        
    movlw	.1            ;Esta es la cuenta atras de la unidad de la via1 cruce rojo 
    subwf	V1CrRojUni,F  ; V1CrRojUni = V1CrRojUni-1  
    btfss	STATUS,C
    goto	DEC_DEC_V1_Roj
    return
DEC_DEC_V1_Roj              ;Esta es la cuenta atras de la Decena de la via1 cruce rojo
    mov     V1CrRojUni,.9   ;Devemos poner el V1CrRojUni en 9 ya que paso de un 10 a un 9
    movlw	.1
    subwf	V1CrRojDec,F
    btfss	STATUS,C
    goto	REINICIAR_DEC_V1_Roj
    return
REINICIAR_DEC_V1_Roj
    
    mov		V1CrRojUni,.0
    mov		V1CrRojDec,.0
    
    return
    ;	  FIN COUNTER_CALC_V1_Roj
    ;===========================
    
    ;===========================
    ;	INICIO COUNTER_CALC_V1_Ver   
COUNTER_CALC_V1_Ver        
    movlw	.1              ;Esta es la cuenta atras de la unidad de la via1 cruce verde
    subwf	V1CrVerdUni,F
    btfss	STATUS,C
    goto	DEC_DEC_V1_Ver
    return
DEC_DEC_V1_Ver              ;Esta es la cuenta atras de la Decena de la via1 cruce verde
    movlw	.9
    movwf	V1CrVerdUni
    movlw	.1
    subwf	V1CrVerdDec,F
    btfss	STATUS,C
    goto	REINICIAR_DEC_V1_Ver
    return
REINICIAR_DEC_V1_Ver
    mov		V1CrVerdUni,.0
    mov		V1CrVerdDec,.0
    
    return
    ;	  FIN COUNTER_CALC_V1_Ver
    ;===========================
    
    ;===========================
    ;	INICIO COUNTER_CALC_V1_Gi_Roj   
COUNTER_CALC_V1_Gi_Roj        
    movlw	.1                  ;Esta es la cuenta atras de la unidad de la via1 giro rojo
    subwf	V1GiRojUni,F
    btfss	STATUS,C
    goto	DEC_DEC_V1_Gi_Roj
    return
DEC_DEC_V1_Gi_Roj               ;Esta es la cuenta atras de la decena de la via1 giro rojo
    
    mov	V1GiRojUni,.9
    movlw	.1
    subwf	V1GiRojDec,F
    btfss	STATUS,C
    goto	REINICIAR_DEC_V1_Gi_Roj
    return
REINICIAR_DEC_V1_Gi_Roj
    
    mov	V1GiRojUni,.0
    mov	V1GiRojDec,.0
    
    return
    ;	  FIN COUNTER_CALC_V1_Gi_Roj
    ;===========================
    
    ;===========================
    ;	INICIO COUNTER_CALC_V1_Gi_Ver   
COUNTER_CALC_V1_Gi_Ver        
    movlw	.1                          ;Esta es la cuenta atras de la unidad de la via1 giro verde
    subwf	V1GiVerdUni,F
    btfss	STATUS,C
    goto	DEC_DEC_V1_Gi_Ver
    return
DEC_DEC_V1_Gi_Ver                       ;Esta es la cuenta atras de la decena de la via1 giro verde
    mov	V1GiVerdUni,.9
    movlw	.1
    subwf	V1GiVerdDec,F
    btfss	STATUS,C
    goto	REINICIAR_DEC_V1_Gi_Ver
    return
REINICIAR_DEC_V1_Gi_Ver
    
    mov	V1GiVerdUni,.0
    mov	V1GiVerdDec,.0
    
    return
    ;	  FIN COUNTER_CALC_V1_Gi_Ver
    ;===========================
    
    ;===========================
    ;	INICIO COUNTER_CALC_V2_Roj   
COUNTER_CALC_V2_Roj        
    movlw	.1                      ;Esta es la cuenta atras de la unidad de la via2 cruce rojo
    subwf	V2CrRojUni,F
    btfss	STATUS,C
    goto	DEC_DEC_V2_Roj
    return
DEC_DEC_V2_Roj                      ;Esta es la cuenta atras de la decena de la via2 cruce rojo
    
    mov	V2CrRojUni,.9
    movlw	.1
    subwf	V2CrRojDec,F
    btfss	STATUS,C
    goto	REINICIAR_DEC_V2_Roj
    return
REINICIAR_DEC_V2_Roj    
    mov		V2CrRojUni,.0
    mov		V2CrRojDec,.0    
    return
    ;	  FIN COUNTER_CALC_V2_Roj
    ;===========================
    
    ;===========================
    ;	INICIO COUNTER_CALC_V2_Ver   
COUNTER_CALC_V2_Ver        
    movlw	.1                      ;Esta es la cuenta atras de la unidad de la via2 cruce verde
    subwf	V2CrVerdUni,F
    btfss	STATUS,C
    goto	DEC_DEC_V2_Ver
    return
DEC_DEC_V2_Ver
                                     ;Esta es la cuenta atras de la decena de la via2 cruce verde
    mov		V2CrVerdUni,.9
    movlw	.1
    subwf	V2CrVerdDec,F
    btfss	STATUS,C
    goto	REINICIAR_DEC_V2_Ver
    return
REINICIAR_DEC_V2_Ver    
    mov		V2CrVerdUni,.0
    mov		V2CrVerdDec,.0    
    return
    ;	  FIN COUNTER_CALC_V2_Ver
    ;===========================
    
    ;===========================
    ;	INICIO COUNTER_CALC_V3_Roj   
COUNTER_CALC_V3_Roj        
    movlw	.1                              ;Esta es la cuenta atras de la unidad de la via3 cruce rojo
    subwf	V3CrRojUni,F
    btfss	STATUS,C
    goto	DEC_DEC_V3_Roj
    return
DEC_DEC_V3_Roj                              ;Esta es la cuenta atras de la decena de la via3 cruce rojo
    mov		V3CrRojUni,.9
    movlw	.1
    subwf	V3CrRojDec,F
    btfss	STATUS,C
    goto	REINICIAR_DEC_V3_Roj
    return
REINICIAR_DEC_V3_Roj    
    mov		V3CrRojUni,.0
    mov		V3CrRojDec,.0    
    return
    ;	  FIN COUNTER_CALC_V3_Roj
    ;===========================
    
    ;===========================
    ;	INICIO COUNTER_CALC_V3_Ver   
COUNTER_CALC_V3_Ver        
    movlw	.1                          ;Esta es la cuenta atras de la unidad de la via3 cruce verde
    subwf	V3CrVerdUni,F
    btfss	STATUS,C
    goto	DEC_DEC_V3_Ver
    return
DEC_DEC_V3_Ver
    
    mov	V3CrVerdUni,.9                  ;Esta es la cuenta atras de la decena de la via3 cruce verde
    movlw	.1
    subwf	V3CrVerdDec,F
    btfss	STATUS,C
    goto	REINICIAR_DEC_V3_Ver
    return
REINICIAR_DEC_V3_Ver
    
    mov	V3CrVerdUni,.0
    mov	V3CrVerdDec,.0
    
    return
    ;	  FIN COUNTER_CALC_V3_Ver
    ;===========================
    
    ;===========================
    ;	INICIO STATUS1   
STATUS1            
    movfw	P_STI1
    movwf	TIME 
    retlw	.2 
    ;	  FIN STATUS1
    ;===========================
    
    ;===========================
    ;	INICIO STATUS2   
STATUS2        
    decfsz	C_TURN      ;Decremento en 1 la cantidad de veces que se repetir� la intermitencia de la luz verde en el Giro de la V�a 1
    goto	STATUS1_INT
    movfw	P_ST3
    movwf	TIME       
    
    movfw	P_V1GiRojDec
    movwf	V1GiRojDec
    movfw	P_V1GiRojUni  
    movwf	V1GiRojUni 
    
    movfw	P_V2CrVerdDec
    movwf	V2CrVerdDec
    movfw	P_V2CrVerdUni  
    movwf	V2CrVerdUni
    
    retlw	.3   
STATUS1_INT
    movfw	P_STI1
    movwf	TIME
    retlw	.1
    
    ;	  FIN STATUS2
    ;===========================
    
    ;===========================
    ;	INICIO STATUS3   
STATUS3       
    movfw	P_ST4
    movwf	TIME    
    retlw	.4   
    ;	  FIN STATUS3
    ;===========================
    
    ;===========================
    ;	INICIO STATUS4   
STATUS4    
    movfw	P_ST5
    movwf	TIME
    
    movfw	P_V1CrRojDec
    movwf	V1CrRojDec
    movfw	P_V1CrRojUni  
    movwf	V1CrRojUni
    
    movfw	P_V2CrRojDec
    movwf	V2CrRojDec
    movfw	P_V2CrRojUni  
    movwf	V2CrRojUni
    
    movfw	P_V3CrVerdDec
    movwf	V3CrVerdDec
    movfw	P_V3CrVerdUni  
    movwf	V3CrVerdUni
    
    retlw	.5
   
    ;	  FIN STATUS4
    ;===========================
    
    ;===========================
    ;	INICIO STATUS5   
STATUS5    
    movfw	P_ST6
    movwf	TIME  
    
    retlw	.6
   
    ;	  FIN STATUS5
    ;===========================
    
    ;===========================
    ;	INICIO STATUS6   
STATUS6    
    movfw	P_C_TURN
    movwf	C_TURN
    movfw	P_ST1
    movwf	TIME
    
    movfw	P_V1CrVerdDec
    movwf	V1CrVerdDec
    movfw	P_V1CrVerdUni  
    movwf	V1CrVerdUni
    movfw	P_V1GiVerdDec
    movwf	V1GiVerdDec
    movfw	P_V1GiVerdUni  
    movwf	V1GiVerdUni 
    movfw	P_V3CrRojDec
    movwf	V3CrRojDec
    movfw	P_V3CrRojUni   
    movwf	V3CrRojUni   
    retlw	.1
   
    ;	  FIN STATUS6
    ;===========================
    
    ;===========================
    ;	INICIO STATUS7   
STATUS7    
    movfw	P_STI2
    movwf	TIME
    
    mov		V1CrRojDec,.0	
    mov		V1CrRojUni,.0
    mov		V1CrVerdDec,.0
    mov		V1CrVerdUni,.0    
    mov		V1GiRojDec,.0
    mov		V1GiRojUni,.0
    mov		V1GiVerdDec,.0    	
    mov		V1GiVerdUni,.0    	
    mov		V2CrRojDec,.0    
    mov		V2CrRojUni,.0    
    mov		V2CrVerdDec,.0	
    mov		V2CrVerdUni,.0	
    mov		V3CrRojDec,.0     
    mov		V3CrRojUni,.0   	
    mov		V3CrVerdDec,.0	
    mov		V3CrVerdUni,.0
    
    retlw	.8
   
    ;	  FIN STATUS7
    ;===========================
    
    ;===========================
    ;	INICIO STATUS8   
STATUS8  
    movf	INT,F
    btfsc	STATUS,Z
    goto	STATUS6
    movfw	P_STI2
    movwf	TIME
    retlw	.7
   
    ;	  FIN STATUS8
    ;===========================

    ;===========================
    ;	INICIO INT_RECEPTION_DATA   
INT_RECEPTION_DATA
       
    banksel	RCREG    
    movfw	RCREG ; resivo el dato y lo coloco en w  
    movlw	.0
    xorwf	BYTE_COUNTER,W
    btfsc	STATUS,Z
    goto	SAVE_INIT_MSJ
    movlw	.1
    xorwf	BYTE_COUNTER,W
    btfsc	STATUS,Z
    goto	SAVE_ID_PARAMETER
    ;movlw	.4
    ;xorwf	BYTE_COUNTER,W
    ;btfsc	STATUS,Z
    ;goto	SAVE_PARAMETER_NIBBLE_H
    movlw	.2
    xorwf	BYTE_COUNTER,W
    btfsc	STATUS,Z
    goto	SAVE_PARAMETER_NIBBLE_L
    movlw	.3
    xorwf	BYTE_COUNTER,W
    btfsc	STATUS,Z
    goto	SAVE_END_MSJ
    
SAVE_INIT_MSJ   
    movfw	RCREG               ;chequea que el primer caracter que le llegue sea un 0x02 si es asi sigue la recepcion si no tira error
    movwf	INIT_MSJ    
    movlw	0x02
    xorwf	INIT_MSJ,W
    btfss	STATUS,Z
    goto	RECEPTION_ERROR
    goto	RECEPTION_SUCCESS
    
SAVE_ID_PARAMETER
    movfw	RCREG               ;paso el dato a ID_PARAMETER para saver que parametro debo modificar
    movwf	ID_PARAMETER
    goto	RECEPTION_SUCCESS
    
SAVE_PARAMETER_NIBBLE_H
    movfw	RCREG               ;cargo el nible alto
    movwf	PARAMETER_NIBBLE_H
    goto	RECEPTION_SUCCESS
    
SAVE_PARAMETER_NIBBLE_L
    movfw	RCREG               ;cargo el nible bajo
    movwf	PARAMETER_NIBBLE_L
    goto	RECEPTION_SUCCESS 
    
SAVE_END_MSJ   
    movfw	RCREG               ;chequea si llegue un 0x03 lo que significa el fin de la transmicion
    movwf	END_MSJ
    movlw	0x03
    xorwf	END_MSJ,W
    btfss	STATUS,Z
    goto	RECEPTION_ERROR
    goto	UPDATE_PARAMETER    
    
RECEPTION_ERROR
    call	RECEPTION_DATA_ERROR
    return
    
RECEPTION_SUCCESS
     
    incf	BYTE_COUNTER    ; BYTE_COUNTER = BYTE_COUNTER + 1
    return
    ;	  FIN INT_RECEPTION_DATA
    ;===========================    
    
    ;===========================
    ;	INICIO RECEPTION_DATA_ERROR
RECEPTION_DATA_ERROR
    clrf    INIT_MSJ            ;limpia todas las variables del apartado recepcion 
    clrf    ID_PARAMETER
    clrf    PARAMETER_NIBBLE_H
    clrf    PARAMETER_NIBBLE_L
    clrf    END_MSJ
    clrf    BYTE_COUNTER
    return
    ;	  FIN RECEPTION_DATA_ERROR
    ;===========================    
    
    ;===========================
    ;	INICIO UPDATE_PARAMETER
UPDATE_PARAMETER    
TEST_UPDATE_P_ST1               ;verifica si se llamo a un cambio de P_ST1, si no pasa a la verificacion el siguigiente parametro
    movlw	P_ST1	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_ST1
    goto	TEST_UPDATE_P_STI1
UPDATE_P_ST1                    ;modifica la variable P_ST1 y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_ST1	
    goto	END_UPDATE	

TEST_UPDATE_P_STI1              ;verifica si se llamo a un cambio de P_STI1, si no pasa a la verificacion el siguigiente parametro
    movlw	P_STI1	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_STI1
    goto	TEST_UPDATE_P_ST3
UPDATE_P_STI1                   ;modifica la variable P_STI1 y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_STI1	
    goto	END_UPDATE	

TEST_UPDATE_P_ST3               ;verifica si se llamo a un cambio de P_ST3, si no pasa a la verificacion el siguigiente parametro
    movlw	P_ST3	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_ST3
    goto	TEST_UPDATE_P_ST4
UPDATE_P_ST3                    ;modifica la variable P_ST3 y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_ST3	
    goto	END_UPDATE	

TEST_UPDATE_P_ST4               ;verifica si se llamo a un cambio de P_ST4, si no pasa a la verificacion el siguigiente parametro
    movlw	P_ST4	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_ST4
    goto	TEST_UPDATE_P_ST5
UPDATE_P_ST4                    ;modifica la variable P_ST4 y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_ST4	
    goto	END_UPDATE	

TEST_UPDATE_P_ST5               ;verifica si se llamo a un cambio de P_ST5, si no pasa a la verificacion el siguigiente parametro
    movlw	P_ST5	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_ST5
    goto	TEST_UPDATE_P_ST6
UPDATE_P_ST5                    ;modifica la variable P_ST5 y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_ST5	
    goto	END_UPDATE	

TEST_UPDATE_P_ST6                ;verifica si se llamo a un cambio de P_ST6, si no pasa a la verificacion el siguigiente parametro
    movlw	P_ST6	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_ST6
    goto	TEST_UPDATE_P_STI2
UPDATE_P_ST6                     ;modifica la variable P_ST6 y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_ST6	
    goto	END_UPDATE	

TEST_UPDATE_P_STI2               ;verifica si se llamo a un cambio de P_STI2, si no pasa a la verificacion el siguigiente parametro
    movlw	P_STI2	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_STI2
    goto	TEST_UPDATE_P_C_TURN
UPDATE_P_STI2                    ;modifica la variable P_STI2 y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_STI2	
    goto	END_UPDATE	

TEST_UPDATE_P_C_TURN            ;verifica si se llamo a un cambio de P_C_TURN, si no pasa a la verificacion el siguigiente parametro
    movlw	P_C_TURN	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_C_TURN
    goto	TEST_UPDATE_INT
UPDATE_P_C_TURN                 ;modifica la variable P_C_TURN y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_C_TURN	
    goto	END_UPDATE	

TEST_UPDATE_INT                 ;verifica si se llamo a un cambio de INT, si no pasa a la verificacion el siguigiente parametro
    movlw	INT	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_INT
    goto	TEST_UPDATE_P_V1CrRojDec
UPDATE_INT                      ;si hubo cambios modifica la variable INT 
    ;banksel	PORTA
    ;bsf		PORTA,RA1
    call	CONVERT_HEXA	
    movwf	INT	
    btfss	STATUS,Z
    goto	SET_STATUS7         
    mov		STAT,.6    
    goto	END_UPDATE          ;Termina la actualizacion
SET_STATUS7
    mov	    STAT,.7             ;se pasa al estado 7
    mov		TIME,.1             ;se comola el TIME en 1 segundo        
    goto	END_UPDATE          ;termina de actualizar
TEST_UPDATE_P_V1CrRojDec        ;verificverifica si se llamo a un cambio de P_V1CrRojDec, si no pasa a la verificacion el siguigiente parametro   
    movlw	P_V1CrRojDec        	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V1CrRojDec
    goto	TEST_UPDATE_P_V1CrRojUni
UPDATE_P_V1CrRojDec             ;modifica la variable P_V1CrRojDec y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V1CrRojDec	
    goto	END_UPDATE	

TEST_UPDATE_P_V1CrRojUni        ;verifica si se llamo a un cambio de P_V1CrRojUni, si no pasa a la verificacion el siguigiente parametro   
    movlw	P_V1CrRojUni	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V1CrRojUni
    goto	TEST_UPDATE_P_V1CrVerdDec
UPDATE_P_V1CrRojUni             ;modifica la variable P_V1CrRojUni y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V1CrRojUni	
    goto	END_UPDATE	

TEST_UPDATE_P_V1CrVerdDec       ;verifica si se llamo a un cambio de P_V1CrVerdDec, si no pasa a la verificacion el siguigiente parametro   
    movlw	P_V1CrVerdDec	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V1CrVerdDec
    goto	TEST_UPDATE_P_V1CrVerdUni
UPDATE_P_V1CrVerdDec            ;modifica la variable P_V1CrVerdDec y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V1CrVerdDec	
    goto	END_UPDATE	

TEST_UPDATE_P_V1CrVerdUni       ;verifica si se llamo a un cambio de P_V1CrVerdUni, si no pasa a la verificacion el siguigiente parametro
    movlw	P_V1CrVerdUni	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V1CrVerdUni
    goto	TEST_UPDATE_P_V1GiRojDec
UPDATE_P_V1CrVerdUni            ;modifica la variable P_V1CrVerdUni y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V1CrVerdUni	
    goto	END_UPDATE	

TEST_UPDATE_P_V1GiRojDec         ;verifica si se llamo a un cambio de P_V1GiRojDec, si no pasa a la verificacion el siguigiente parametro
    movlw	P_V1GiRojDec	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V1GiRojDec
    goto	TEST_UPDATE_P_V1GiRojUni
UPDATE_P_V1GiRojDec              ;modifica la variable P_V1GiRojDec y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V1GiRojDec	
    goto	END_UPDATE	

TEST_UPDATE_P_V1GiRojUni          ;verifica si se llamo a un cambio de P_V1GiRojUni, si no pasa a la verificacion el siguigiente parametro
    movlw	P_V1GiRojUni	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V1GiRojUni
    goto	TEST_UPDATE_P_V1GiVerdDec
UPDATE_P_V1GiRojUni                ;modifica la variable P_V1GiRojUni y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V1GiRojUni	
    goto	END_UPDATE	

TEST_UPDATE_P_V1GiVerdDec          ;verifica si se llamo a un cambio de P_V1GiVerdDec, si no pasa a la verificacion el siguigiente parametro
    movlw	P_V1GiVerdDec	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V1GiVerdDec
    goto	TEST_UPDATE_P_V1GiVerdUni
UPDATE_P_V1GiVerdDec                ;modifica la variable P_V1GiVerdDec y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V1GiVerdDec	
    goto	END_UPDATE	

TEST_UPDATE_P_V1GiVerdUni           ;verifica si se llamo a un cambio de P_V1GiVerdUni, si no pasa a la verificacion el siguigiente parametro
    movlw	P_V1GiVerdUni	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V1GiVerdUni
    goto	TEST_UPDATE_P_V2CrRojDec
UPDATE_P_V1GiVerdUni                ;modifica la variable P_V1GiVerdUni y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V1GiVerdUni	
    goto	END_UPDATE	

TEST_UPDATE_P_V2CrRojDec            ;verifica si se llamo a un cambio de P_V2CrRojDec , si no pasa a la verificacion el siguigiente parametro
    movlw	P_V2CrRojDec	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V2CrRojDec
    goto	TEST_UPDATE_P_V2CrRojUni
UPDATE_P_V2CrRojDec                 ;modifica la variable P_V2CrRojDec y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V2CrRojDec	
    goto	END_UPDATE	

TEST_UPDATE_P_V2CrRojUni            ;verifica si se llamo a un cambio de P_V2CrRojUni , si no pasa a la verificacion el siguigiente parametro
    movlw	P_V2CrRojUni	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V2CrRojUni
    goto	TEST_UPDATE_P_V2CrVerdDec
UPDATE_P_V2CrRojUni                 ;modifica la variable P_V2CrRojUni y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V2CrRojUni	
    goto	END_UPDATE	

TEST_UPDATE_P_V2CrVerdDec           ;verifica si se llamo a un cambio de P_V2CrVerdDec , si no pasa a la verificacion el siguigiente parametro
    movlw	P_V2CrVerdDec	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V2CrVerdDec
    goto	TEST_UPDATE_P_V2CrVerdUni
UPDATE_P_V2CrVerdDec                ;modifica la variable P_V2CrVerdDec y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V2CrVerdDec	
    goto	END_UPDATE	

TEST_UPDATE_P_V2CrVerdUni           ;verifica si se llamo a un cambio de P_V2CrVerdUni, si no pasa a la verificacion el siguigiente parametro
    movlw	P_V2CrVerdUni	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V2CrVerdUni
    goto	TEST_UPDATE_P_V3CrRojDec
UPDATE_P_V2CrVerdUni                ;modifica la variable P_V2CrVerdUni y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V2CrVerdUni	
    goto	END_UPDATE	

TEST_UPDATE_P_V3CrRojDec            ;verifica si se llamo a un cambio de P_V3CrRojDec, si no pasa a la verificacion el siguigiente parametro
    movlw	P_V3CrRojDec	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V3CrRojDec
    goto	TEST_UPDATE_P_V3CrRojUni
UPDATE_P_V3CrRojDec                 ;modifica la variable P_V3CrRojDec y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V3CrRojDec	
    goto	END_UPDATE	

TEST_UPDATE_P_V3CrRojUni            ;verifica si se llamo a un cambio de P_V3CrRojUni, si no pasa a la verificacion el siguigiente parametro
    movlw	P_V3CrRojUni	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V3CrRojUni
    goto	TEST_UPDATE_P_V3CrVerdDec
UPDATE_P_V3CrRojUni                 ;modifica la variable P_V3CrRojUni y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V3CrRojUni	
    goto	END_UPDATE	

TEST_UPDATE_P_V3CrVerdDec           ;verifica si se llamo a un cambio de P_V3CrVerdDec, si no pasa a la verificacion el siguigiente parametro
    movlw	P_V3CrVerdDec	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V3CrVerdDec
    goto	TEST_UPDATE_P_V3CrVerdUni
UPDATE_P_V3CrVerdDec                ;modifica la variable P_V3CrVerdDec y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V3CrVerdDec	
    goto	END_UPDATE	

TEST_UPDATE_P_V3CrVerdUni           ;verifica si se llamo a un cambio de P_V3CrVerdUni, si no pasa a la verificacion el siguigiente parametro
    movlw	P_V3CrVerdUni	
    xorwf	ID_PARAMETER,W	
    btfsc	STATUS,Z	
    goto	UPDATE_P_V3CrVerdUni
    call	RECEPTION_DATA_ERROR
    return
UPDATE_P_V3CrVerdUni                ;modifica la variable P_V3CrVerdUni y termina la actualizacion
    call	CONVERT_HEXA	
    movwf	P_V3CrVerdUni	
    goto	END_UPDATE
    
END_UPDATE    
    clrf	BYTE_COUNTER
    return
    ;	  FIN UPDATE_PARAMETER
    ;===========================    
    ;===========================
    ;	INICIO CONVERT_HEXA
CONVERT_HEXA
    ;movlw	0x0F
    ;andwf	PARAMETER_NIBBLE_H,F
    ;andwf	PARAMETER_NIBBLE_L,F
    ;swapf	PARAMETER_NIBBLE_H,W
    movfw	PARAMETER_NIBBLE_L 
    return	
    ;	  FIN CONVERT_HEXA
    ;===========================  
    
    ;===========================
    ;	INICIO CONVERT_BCD
CONVERT_BCD
    movlw	0x0F
    andwf	PARAMETER_NIBBLE_L,W    
    return
    ;	  FIN CONVERT_BCD
    ;===========================    
    
    ;===========================
    ;	INICIO INIT_INT_SEND_DATA
INIT_INT_SEND_DATA
    banksel	PIE1
    bsf		PIE1,TXIE           ;Habilito la interrupcion de datos de la EUSAR
    bankS	.0
    mov		BYTE_SEND_COUNTER,.0
    
INT_SEND_DATA    
    banksel	PIE1
    btfss	PIE1,TXIE           ;si no esta habilitada la interrupcion retorna
    return
    bankS	.0
    movfw	BYTE_SEND_COUNTER
    call	BYTE_TO_SEND
    movwf	BYTE_SEND       
    movlw	0x0A        	
    xorwf	BYTE_SEND,W     	
    btfsc	STATUS,Z           ;pregunta si hubo un salto de linea
    goto	END_INT_SEND_DATA
    banksel	TXREG
    movr	TXREG,BYTE_SEND     ;mandas el dato a TXREG (salida por puerto RC6) e incrementas BYTE_SEND_COUNTER
    incf	BYTE_SEND_COUNTER
    return 
END_INT_SEND_DATA    
    banksel	TXREG
    movr	TXREG,BYTE_SEND     ;Movemos lo que hay en BYTE_SEND a TXREG e incrementas BYTE_SEND_COUNTER
    incf	BYTE_SEND_COUNTER
    banksel	PIE1
    bcf		PIE1,TXIE           ;deshabilito la interrupcion 
    bankS	.0
    return
    
    ;	  FIN INIT_INT_SEND_DATA
    ;===========================    
    
    ;===========================
    ;	INICIO BYTE_TO_SEND
BYTE_TO_SEND                      ;Del (SEND:SEND_2) se enviaran "<"    
    movlw	.0	
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z
    goto	BYTE_TO_SEND_1    
    RETLW	0x3C
BYTE_TO_SEND_1    
    movlw	.1	
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z
    goto	BYTE_TO_SEND_2    
    RETLW	0x3C
BYTE_TO_SEND_2    
    movlw	.2	
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z
    goto	BYTE_TO_SEND_3    
    RETLW	0x3C
BYTE_TO_SEND_3                  ;Del (SEND_3:SEND_28) se envian todos los parametros de importancia para la central     
    movlw	.3	
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z
    goto	BYTE_TO_SEND_4    	
    movfw	STAT
    return
BYTE_TO_SEND_4    
    movlw	.4	
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z
    goto	BYTE_TO_SEND_5    	
    movfw	P_ST1
    return  
BYTE_TO_SEND_5
    movlw	.5
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_6
    movfw	P_STI1
    return
BYTE_TO_SEND_6
    movlw	.6
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_7
    movfw	P_ST3
    return
BYTE_TO_SEND_7
    movlw	.7
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_8
    movfw	P_ST4
    return
BYTE_TO_SEND_8
    movlw	.8
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_9
    movfw	P_ST5
    return
BYTE_TO_SEND_9
    movlw	.9
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_10
    movfw	P_ST6
    return
BYTE_TO_SEND_10
    movlw	.10
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_11
    movfw	P_STI2   
    return
BYTE_TO_SEND_11
    movlw	.11
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_12
    movfw	V1CrRojDec
    return
BYTE_TO_SEND_12
    movlw	.12
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_13
    movfw	V1CrRojUni
    return
BYTE_TO_SEND_13
    movlw	.13
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_14
    movfw	V1CrVerdDec
    return
BYTE_TO_SEND_14
    movlw	.14
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_15
    movfw	V1CrVerdUni
    return
BYTE_TO_SEND_15
    movlw	.15
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_16
    movfw	V1GiRojDec
    return
BYTE_TO_SEND_16
    movlw	.16
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_17
    movfw	V1GiRojUni
    return
BYTE_TO_SEND_17
    movlw	.17
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_18
    movfw	V1GiVerdDec
    return
BYTE_TO_SEND_18
    movlw	.18
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_19
    movfw	V1GiVerdUni
    return
BYTE_TO_SEND_19
    movlw	.19
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_20
    movfw	V2CrRojDec
    return
BYTE_TO_SEND_20
    movlw	.20
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_21
    movfw	V2CrRojUni
    return
BYTE_TO_SEND_21
    movlw	.21
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_22
    movfw	V2CrVerdDec
    return
BYTE_TO_SEND_22
    movlw	.22
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_23
    movfw	V2CrVerdUni
    return
BYTE_TO_SEND_23
    movlw	.23
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_24
    movfw	V3CrRojDec
    return
BYTE_TO_SEND_24
    movlw	.24
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_25
    movfw	V3CrRojUni
    return
BYTE_TO_SEND_25
    movlw	.25
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_26
    movfw	V3CrVerdDec
    return
BYTE_TO_SEND_26
    movlw	.26
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_27
    movfw	V3CrVerdUni
    return
BYTE_TO_SEND_27
    movlw	.27
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_28
    movfw	P_C_TURN
    return
BYTE_TO_SEND_28
    movlw	.28
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z	
    goto	BYTE_TO_SEND_29
    movfw	INT
    return
BYTE_TO_SEND_29                     ;Del (SEND_29:SEND_31) se enviaran ">"   
    movlw	.29	
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z
    goto	BYTE_TO_SEND_30    
    RETLW	0x3E
BYTE_TO_SEND_30    
    movlw	.30	
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z
    goto	BYTE_TO_SEND_31    
    RETLW	0x3E
BYTE_TO_SEND_31    
    movlw	.31	
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z
    goto	BYTE_TO_SEND_32    
    RETLW	0x3E
BYTE_TO_SEND_32        
    movlw	.32         ;si el BYTE_SEND_COUNTER=32 retorna LF (salto de linea) si no un espacio
    xorwf	BYTE_SEND_COUNTER,W	
    btfss	STATUS,Z
    RETLW	0xFF    
    RETLW	0x0A    
   
     ;	  FIN BYTE_TO_SEND
    ;===========================    
    end
    