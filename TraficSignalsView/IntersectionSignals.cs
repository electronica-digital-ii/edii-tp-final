﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

namespace TraficSignalsView
{
    public class IntersectionSignals
    {
        public int Status { get; set; }

        public int PicParamSTAT { get; set; }
        public int PicParamST1 { get; set; }
        public int PicParamSTI1 { get; set; }
        public int PicParamST3 { get; set; }
        public int PicParamST4 { get; set; }
        public int PicParamST5 { get; set; }
        public int PicParamST6 { get; set; }
        public int PicParamSTI2 { get; set; }
        public int PicParamCTurn { get; set; }
        public int PicParamInt { get; set; }

        public int PicParamST1Send { get; set; }
        public int PicParamSTI1Send { get; set; }
        public int PicParamST3Send { get; set; }
        public int PicParamST4Send { get; set; }
        public int PicParamST5Send { get; set; }
        public int PicParamST6Send { get; set; }
        public int PicParamSTI2Send { get; set; }
        public int PicParamCTurnSend { get; set; }
        public int PicParamIntSend { get; set; }

        public int V1CruceRojo { get; set; }
        public int V1CruceVerde { get; set; }
        public int V1GiroRojo { get; set; }
        public int V1GiroVerde { get; set; }
        public int V2CruceRojo { get; set; }
        public int V2CruceVerde { get; set; }
        public int V3CruceRojo { get; set; }
        public int V3CruceVerde { get; set; }
        public bool Conected { get; internal set; }

        private SerialPortManagger serialPort;

        public IntersectionSignals()
        {
            Conected = false;
            serialPort = new SerialPortManagger(_serialPort_DataReceived);
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var byteReceived = Encoding.ASCII.GetBytes(((SerialPort)sender).ReadLine());
            Conected = true;

            if (byteReceived.Length < 29) return;

            PicParamSTAT = Status = byteReceived[3];

            V1CruceRojo = byteReceived[11] * 10 + byteReceived[12];
            V1CruceVerde = byteReceived[13] * 10 + byteReceived[14];
            V1GiroRojo = byteReceived[15] * 10 + byteReceived[16];
            V1GiroVerde = byteReceived[17] * 10 + byteReceived[18];
            V2CruceRojo = byteReceived[19] * 10 + byteReceived[20];
            V2CruceVerde = byteReceived[21] * 10 + byteReceived[22];
            V3CruceRojo = byteReceived[23] * 10 + byteReceived[24];
            V3CruceVerde = byteReceived[25] * 10 + byteReceived[26];

            PicParamST1 = byteReceived[4];
            PicParamSTI1 = byteReceived[5];
            PicParamST3 = byteReceived[6];
            PicParamST4 = byteReceived[7];
            PicParamST5 = byteReceived[8];
            PicParamST6 = byteReceived[9];
            PicParamSTI2 = byteReceived[10];
            PicParamCTurn = byteReceived[27];
            PicParamInt = byteReceived[28];

        }

        public void SendParam(PicParam picParam)
        {
            serialPort.SendParam(picParam);
        }
    }


}
