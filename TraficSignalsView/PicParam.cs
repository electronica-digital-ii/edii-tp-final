﻿namespace TraficSignalsView
{
    public class PicParam
    {
        public PicParamType TypeParam { get; set; }
        public byte NibbleH { get; set; }
        public byte NibbleL { get; set; }

        public PicParam(PicParamType type,  byte nibbleL)
        {
            TypeParam = type;
            NibbleH = 0;
            NibbleL = nibbleL;
        }
        public PicParam(PicParamType type,  int nibbleL)
        {
            TypeParam = type;
            NibbleH = 0;
            NibbleL = (byte)nibbleL;
        }
    }

}
