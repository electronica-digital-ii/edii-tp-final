﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using TraficSignalsView.Properties;

namespace TraficSignalsView
{
    public partial class Form1 : Form
    {
        System.Windows.Forms.Timer Timer;
        private IntersectionSignals Intersection;
        private bool SendCounterValues;
        public Form1()
        {
            InitializeComponent();
            Intersection = new IntersectionSignals();
            SendCounterValues = false;
            DrawOff();
            InitTimer();
        }

        private void InitTimer()
        {
            Timer = new System.Windows.Forms.Timer();
            Timer.Enabled = false;
            Timer.Interval = 250;
            Timer.Tick += Draw;
            Timer.Enabled = true;
        }

        private void Draw(object sender, EventArgs e)
        {
            if (!Intersection.Conected) { DrawOff(); return; }
            Timer.Enabled = false;
            DrawStatus();

            Timer.Enabled = true;
        }

        private void DrawStatus()
        {
            DrawCrossVia1();
            DrawTurnVia1();
            DrawCrossVia2();
            DrawCrossVia3();
            SetPicParameters();
        }

        private void SetPicParameters()
        {
            TxtPicParamSTAT.Text = Intersection.PicParamSTAT.ToString();
            TxtPicParamST1.Text = Intersection.PicParamST1.ToString();
            TxtPicParamSTI1.Text = Intersection.PicParamSTI1.ToString();
            TxtPicParamSTI2.Text = Intersection.PicParamSTI2.ToString();
            TxtPicParamST3.Text = Intersection.PicParamST3.ToString();
            TxtPicParamST4.Text = Intersection.PicParamST4.ToString();
            TxtPicParamST5.Text = Intersection.PicParamST5.ToString();
            TxtPicParamST6.Text = Intersection.PicParamST6.ToString();
            TxtPicParamCTurn.Text = Intersection.PicParamCTurn.ToString();
            TxtPicParamInt.Text = Intersection.PicParamInt.ToString();
        }

        private void DrawCrossVia1()
        {
            SignalCrossVia1.Image = Intersection.Status <= 3
                                        ? Resources.Semaphore_Green
                                        : Intersection.Status == 4
                                          || Intersection.Status == 7
                                            ? Resources.Semaphore_Yellow
                                            : Intersection.Status == 5
                                          || Intersection.Status == 6
                                          ? Resources.Semaphore_Red
                                          : Resources.Semaphore_Off;

            CounterVia1CrossRed.Text = Intersection.V1CruceRojo.ToString("00");
            CounterVia1CrossGreen.Text = Intersection.V1CruceVerde.ToString("00");
        }

        private void DrawTurnVia1()
        {
            SignalTurnVia1.Image = Intersection.Status == 1
                                        ? Resources.TurnLeftSemaphore_Green
                                        : Intersection.Status == 2
                                          || Intersection.Status == 8
                                            ? Resources.TurnLeftSemaphore_Off
                                            : Intersection.Status >= 3
                                          || Intersection.Status <= 7
                                          ? Resources.TurnLeftSemaphore_Red
                                          : Resources.TurnLeftSemaphore_Off;

            CounterVia1TurnRed.Text = Intersection.V1GiroRojo.ToString("00");
            CounterVia1TurnGreen.Text = Intersection.V1GiroVerde.ToString("00");
        }

        private void DrawCrossVia2()
        {
            SignalCrossVia2.Image = Intersection.Status == 4
                                    || Intersection.Status == 7
                                        ? Resources.Semaphore_Yellow
                                        : Intersection.Status == 8
                                            ? Resources.Semaphore_Off
                                            : Intersection.Status == 3
                                            ? Resources.Semaphore_Green
                                            : Resources.Semaphore_Red;

            CounterVia2CrossRed.Text = Intersection.V2CruceRojo.ToString("00");
            CounterVia2CrossGreen.Text = Intersection.V2CruceVerde.ToString("00");
        }

        private void DrawCrossVia3()
        {
            SignalCrossVia3.Image = Intersection.Status == 5
                                        ? Resources.Semaphore_Green
                                        : Intersection.Status == 6
                                            ? Resources.Semaphore_Yellow
                                            : Intersection.Status == 8
                                          ? Resources.Semaphore_Off
                                          : Resources.Semaphore_Red;

            CounterVia3CrossRed.Text = Intersection.V3CruceRojo.ToString("00");
            CounterVia3CrossGreen.Text = Intersection.V3CruceVerde.ToString("00");
        }

        private void DrawOff()
        {
            SignalCrossVia1.Image = Resources.Semaphore_Off;
            SignalTurnVia1.Image = Resources.TurnLeftSemaphore_Off;
            SignalCrossVia2.Image = Resources.Semaphore_Off;
            SignalCrossVia3.Image = Resources.Semaphore_Off;

            CounterVia1TurnGreen.Text = "  ";
            CounterVia1TurnRed.Text = "  ";
            CounterVia1CrossGreen.Text = "  ";
            CounterVia1CrossRed.Text = "  ";
            CounterVia2CrossGreen.Text = "  ";
            CounterVia2CrossRed.Text = "  ";
            CounterVia3CrossGreen.Text = "  ";
            CounterVia3CrossRed.Text = "  ";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SendCounterValues = false;
            SendParamST1();
            SendParamSTI1();
            SendParamST3();
            SendParamST4();
            SendParamST5();
            SendParamST6();
            SendParamSTI2();
            SendParamCTurn();
            SendParamInt();
            if (SendCounterValues) SendCounterParam();
        }

        private void SendCounterParam()
        {
            var paramST1 = ToInt(TxtParamST1.Text) ?? ToInt(TxtPicParamST1.Text);
            var paramSTI1 = ToInt(TxtParamSTI1.Text) ?? ToInt(TxtPicParamSTI1.Text);
            var paramST3 = ToInt(TxtParamST3.Text) ?? ToInt(TxtPicParamST3.Text);
            var paramST4 = ToInt(TxtParamST4.Text) ?? ToInt(TxtPicParamST4.Text);
            var paramST5 = ToInt(TxtParamST5.Text) ?? ToInt(TxtPicParamST5.Text);
            var paramST6 = ToInt(TxtParamST6.Text) ?? ToInt(TxtPicParamST6.Text);
            var paramSTI2 = ToInt(TxtParamSTI2.Text) ?? ToInt(TxtPicParamSTI2.Text);
            var paramCTurn = ToInt(TxtParamCTurn.Text) ?? ToInt(TxtPicParamCTurn.Text);
            var paramInt = ToInt(TxtParamInt.Text) ?? ToInt(TxtPicParamInt.Text);

            var timeV1CrossGreen = paramInt == 0 ? paramST1 + (paramCTurn * 2 * paramSTI1) + paramST3 : 0;
            var timeV1CrossRed = paramInt == 0 ? paramST5 + paramST6 : 0;
            var timeV1TurnRed = paramInt == 0 ? paramST3 + paramST4 + paramST5 + paramST6 : 0;
            var timeV1TurnGreen = paramInt == 0 ? paramST1 + (paramCTurn * 2 * paramSTI1) : 0;
            var timeV2CrossRed = paramInt == 0 ? paramST1 + (paramCTurn * 2 * paramSTI1) + paramST5 + paramST6 : 0;
            var timeV2CrossGreen = paramInt == 0 ? paramST3 : 0;
            var timeV3CrossRed = paramInt == 0 ? paramST1 + (paramCTurn * 2 * paramSTI1) + paramST3 + paramST4 : 0;
            var timeV3CrossGreen = paramInt == 0 ? paramST5 : 0;


            var nibblesV1CrossGreen = SepareInDecenas(timeV1CrossGreen.Value);
            Intersection.SendParam(new PicParam(PicParamType.P_V1CrVerdDec, nibblesV1CrossGreen[0]));
            Intersection.SendParam(new PicParam(PicParamType.P_V1CrVerdUni, nibblesV1CrossGreen[1]));

            var nibblesV1CrossRed = SepareInDecenas(timeV1CrossRed.Value);
            Intersection.SendParam(new PicParam(PicParamType.P_V1CrRojDec, nibblesV1CrossRed[0]));
            Intersection.SendParam(new PicParam(PicParamType.P_V1CrRojUni, nibblesV1CrossRed[1]));

            var nibblesV1TurnGreen = SepareInDecenas(timeV1TurnGreen.Value);
            Intersection.SendParam(new PicParam(PicParamType.P_V1GiVerdDec, nibblesV1TurnGreen[0]));
            Intersection.SendParam(new PicParam(PicParamType.P_V1GiVerdUni, nibblesV1TurnGreen[1]));

            var nibblesV1TurnRed = SepareInDecenas(timeV1TurnRed.Value);
            Intersection.SendParam(new PicParam(PicParamType.P_V1GiRojDec, nibblesV1TurnRed[0]));
            Intersection.SendParam(new PicParam(PicParamType.P_V1GiRojUni, nibblesV1TurnRed[1]));

            var nibblesV2CrossGreen = SepareInDecenas(timeV2CrossGreen.Value);
            Intersection.SendParam(new PicParam(PicParamType.P_V2CrVerdDec, nibblesV2CrossGreen[0]));
            Intersection.SendParam(new PicParam(PicParamType.P_V2CrVerdUni, nibblesV2CrossGreen[1]));

            var nibblesV2CrossRed = SepareInDecenas(timeV2CrossRed.Value);
            Intersection.SendParam(new PicParam(PicParamType.P_V2CrRojDec, nibblesV2CrossRed[0]));
            Intersection.SendParam(new PicParam(PicParamType.P_V2CrRojUni, nibblesV2CrossRed[1]));

            var nibblesV3CrossGreen = SepareInDecenas(timeV3CrossGreen.Value);
            Intersection.SendParam(new PicParam(PicParamType.P_V3CrVerdDec, nibblesV3CrossGreen[0]));
            Intersection.SendParam(new PicParam(PicParamType.P_V3CrVerdUni, nibblesV3CrossGreen[1]));

            var nibblesV3CrossRed = SepareInDecenas(timeV3CrossRed.Value);
            Intersection.SendParam(new PicParam(PicParamType.P_V3CrRojDec, nibblesV3CrossRed[0]));
            Intersection.SendParam(new PicParam(PicParamType.P_V3CrRojUni, nibblesV3CrossRed[1]));

            SendCounterValues = false;
        }

        private void SendParamInt()
        {
            var paramInt = ToInt(TxtParamInt.Text);
            if (paramInt == null) return;
            Intersection.SendParam(new PicParam(PicParamType.INT, paramInt.Value));
            SendCounterValues = true;
        }

        private void SendParamCTurn()
        {
            var paramCTurn = ToInt(TxtParamCTurn.Text);
            if (paramCTurn == null) return;
            Intersection.SendParam(new PicParam(PicParamType.P_C_TURN, paramCTurn.Value));
            SendCounterValues = true;
        }

        private void SendParamSTI2()
        {
            var paramSTI2 = ToInt(TxtParamSTI2.Text);
            if (paramSTI2 == null) return;
            Intersection.SendParam(new PicParam(PicParamType.P_STI2, paramSTI2.Value));
            SendCounterValues = true;
        }

        private void SendParamST6()
        {
            var paramST6 = ToInt(TxtParamST6.Text);
            if (paramST6 == null) return;
            Intersection.SendParam(new PicParam(PicParamType.P_ST6, paramST6.Value));
            SendCounterValues = true;
        }

        private void SendParamST5()
        {
            var paramST5 = ToInt(TxtParamST5.Text);
            if (paramST5 == null) return;
            Intersection.SendParam(new PicParam(PicParamType.P_ST5, paramST5.Value));
            SendCounterValues = true;
        }

        private void SendParamST4()
        {
            var paramST4 = ToInt(TxtParamST4.Text);
            if (paramST4 == null) return;
            Intersection.SendParam(new PicParam(PicParamType.P_ST4, paramST4.Value));
            SendCounterValues = true;
        }

        private void SendParamST3()
        {
            var paramST3 = ToInt(TxtParamST3.Text);
            if (paramST3 == null) return;
            Intersection.SendParam(new PicParam(PicParamType.P_ST3, paramST3.Value));
            SendCounterValues = true;
        }

        private void SendParamSTI1()
        {
            var paramSTI1 = ToInt(TxtParamSTI1.Text);
            if (paramSTI1 == null) return;
            Intersection.SendParam(new PicParam(PicParamType.P_STI1, paramSTI1.Value));
            SendCounterValues = true;
        }

        private void SendParamST1()
        {
            var paramST1 = ToInt(TxtParamST1.Text);
            if (paramST1 == null) return;
            Intersection.SendParam(new PicParam(PicParamType.P_ST1, paramST1.Value));
            SendCounterValues = true;
        }

        public byte[] GetParamNibbles(string value)
        {
            var param = ToInt(value);
            if (param == null) return null;
            return SepareInDecenas(param.Value);
        }

        public byte[] SepareInDecenas(int value)
        {
            var nibbleH = value / 10;
            var nibbleL = value - (nibbleH * 10);
            return new byte[] { (byte)nibbleH, (byte)nibbleL };
        }

        public int? ToInt(string val)
        {
            if (!int.TryParse(val, out int res)) return null;
            return res;
        }
    }
}
