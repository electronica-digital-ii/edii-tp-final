﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraficSignalsView
{
    public enum PicParamType
    {
        P_ST1 = 0x50,
        P_STI1,
        P_ST3,
        P_ST4,
        P_ST5,
        P_ST6,
        P_STI2,
        P_C_TURN,
        P_V1CrRojDec,
        P_V1CrRojUni,
        P_V1CrVerdDec,
        P_V1CrVerdUni,
        P_V1GiRojDec,
        P_V1GiRojUni,
        P_V1GiVerdDec,
        P_V1GiVerdUni,
        P_V2CrRojDec,
        P_V2CrRojUni,
        P_V2CrVerdDec,
        P_V2CrVerdUni,
        P_V3CrRojDec,
        P_V3CrRojUni,
        P_V3CrVerdDec,
        P_V3CrVerdUni,
        INT = 0x39,
    }

}
