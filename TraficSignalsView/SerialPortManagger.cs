﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace TraficSignalsView
{
    public class SerialPortManagger
    {
        private SerialPort _serialPort;
        public SerialPortManagger(SerialDataReceivedEventHandler receivedEvent)
        {

            _serialPort = new SerialPort();

            // Allow the user to set the appropriate properties.
            _serialPort.PortName = "COM2";
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            _serialPort.DataBits = 8;
            _serialPort.StopBits = StopBits.One;
            _serialPort.Handshake = Handshake.None;

            _serialPort.DataReceived += receivedEvent;

            _serialPort.Open();
        }

        public void SendParam(PicParam picParam)
        {
            var res = new Byte[] { 0x02, (byte)picParam.TypeParam,  picParam.NibbleL, 0x03 };
            _serialPort.Write(res, 0, res.Count());
        }

    }

}
