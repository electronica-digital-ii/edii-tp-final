﻿namespace TraficSignalsView
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.SignalCrossVia3 = new System.Windows.Forms.PictureBox();
            this.SignalCrossVia2 = new System.Windows.Forms.PictureBox();
            this.SignalCrossVia1 = new System.Windows.Forms.PictureBox();
            this.StreetsImg = new System.Windows.Forms.PictureBox();
            this.SignalTurnVia1 = new System.Windows.Forms.PictureBox();
            this.CounterVia1CrossRed = new System.Windows.Forms.Label();
            this.CounterVia2CrossRed = new System.Windows.Forms.Label();
            this.CounterVia3CrossRed = new System.Windows.Forms.Label();
            this.CounterVia1CrossGreen = new System.Windows.Forms.Label();
            this.CounterVia1TurnGreen = new System.Windows.Forms.Label();
            this.CounterVia1TurnRed = new System.Windows.Forms.Label();
            this.CounterVia3CrossGreen = new System.Windows.Forms.Label();
            this.CounterVia2CrossGreen = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TxtPicParamSTAT = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtParamInt = new System.Windows.Forms.TextBox();
            this.TxtPicParamInt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtParamCTurn = new System.Windows.Forms.TextBox();
            this.TxtPicParamCTurn = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtParamSTI2 = new System.Windows.Forms.TextBox();
            this.TxtPicParamSTI2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtParamST6 = new System.Windows.Forms.TextBox();
            this.TxtPicParamST6 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtParamST5 = new System.Windows.Forms.TextBox();
            this.TxtPicParamST5 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtParamST4 = new System.Windows.Forms.TextBox();
            this.TxtPicParamST4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtParamST3 = new System.Windows.Forms.TextBox();
            this.TxtPicParamST3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtParamSTI1 = new System.Windows.Forms.TextBox();
            this.TxtPicParamSTI1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtParamST1 = new System.Windows.Forms.TextBox();
            this.TxtPicParamST1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SignalCrossVia3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignalCrossVia2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignalCrossVia1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StreetsImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignalTurnVia1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SignalCrossVia3
            // 
            this.SignalCrossVia3.BackColor = System.Drawing.Color.Gray;
            this.SignalCrossVia3.Image = global::TraficSignalsView.Properties.Resources.Semaphore_Off;
            this.SignalCrossVia3.Location = new System.Drawing.Point(50, 233);
            this.SignalCrossVia3.Name = "SignalCrossVia3";
            this.SignalCrossVia3.Size = new System.Drawing.Size(21, 51);
            this.SignalCrossVia3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.SignalCrossVia3.TabIndex = 3;
            this.SignalCrossVia3.TabStop = false;
            // 
            // SignalCrossVia2
            // 
            this.SignalCrossVia2.BackColor = System.Drawing.Color.Gray;
            this.SignalCrossVia2.Image = global::TraficSignalsView.Properties.Resources.Semaphore_Off;
            this.SignalCrossVia2.Location = new System.Drawing.Point(186, 346);
            this.SignalCrossVia2.Name = "SignalCrossVia2";
            this.SignalCrossVia2.Size = new System.Drawing.Size(21, 51);
            this.SignalCrossVia2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.SignalCrossVia2.TabIndex = 2;
            this.SignalCrossVia2.TabStop = false;
            // 
            // SignalCrossVia1
            // 
            this.SignalCrossVia1.BackColor = System.Drawing.Color.Gray;
            this.SignalCrossVia1.Image = global::TraficSignalsView.Properties.Resources.Semaphore_Off;
            this.SignalCrossVia1.Location = new System.Drawing.Point(380, 116);
            this.SignalCrossVia1.Name = "SignalCrossVia1";
            this.SignalCrossVia1.Size = new System.Drawing.Size(21, 51);
            this.SignalCrossVia1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.SignalCrossVia1.TabIndex = 1;
            this.SignalCrossVia1.TabStop = false;
            // 
            // StreetsImg
            // 
            this.StreetsImg.Image = global::TraficSignalsView.Properties.Resources.Streets;
            this.StreetsImg.Location = new System.Drawing.Point(12, 12);
            this.StreetsImg.Name = "StreetsImg";
            this.StreetsImg.Size = new System.Drawing.Size(633, 443);
            this.StreetsImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.StreetsImg.TabIndex = 0;
            this.StreetsImg.TabStop = false;
            // 
            // SignalTurnVia1
            // 
            this.SignalTurnVia1.BackColor = System.Drawing.Color.Gainsboro;
            this.SignalTurnVia1.Image = global::TraficSignalsView.Properties.Resources.TurnLeftSemaphore_Off;
            this.SignalTurnVia1.Location = new System.Drawing.Point(291, 135);
            this.SignalTurnVia1.Name = "SignalTurnVia1";
            this.SignalTurnVia1.Size = new System.Drawing.Size(21, 32);
            this.SignalTurnVia1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.SignalTurnVia1.TabIndex = 4;
            this.SignalTurnVia1.TabStop = false;
            // 
            // CounterVia1CrossRed
            // 
            this.CounterVia1CrossRed.AutoSize = true;
            this.CounterVia1CrossRed.BackColor = System.Drawing.Color.Black;
            this.CounterVia1CrossRed.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CounterVia1CrossRed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.CounterVia1CrossRed.Location = new System.Drawing.Point(407, 125);
            this.CounterVia1CrossRed.Name = "CounterVia1CrossRed";
            this.CounterVia1CrossRed.Size = new System.Drawing.Size(30, 17);
            this.CounterVia1CrossRed.TabIndex = 5;
            this.CounterVia1CrossRed.Text = "05";
            this.CounterVia1CrossRed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CounterVia2CrossRed
            // 
            this.CounterVia2CrossRed.AutoSize = true;
            this.CounterVia2CrossRed.BackColor = System.Drawing.Color.Black;
            this.CounterVia2CrossRed.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CounterVia2CrossRed.ForeColor = System.Drawing.Color.Red;
            this.CounterVia2CrossRed.Location = new System.Drawing.Point(213, 356);
            this.CounterVia2CrossRed.Name = "CounterVia2CrossRed";
            this.CounterVia2CrossRed.Size = new System.Drawing.Size(30, 17);
            this.CounterVia2CrossRed.TabIndex = 6;
            this.CounterVia2CrossRed.Text = "05";
            this.CounterVia2CrossRed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CounterVia3CrossRed
            // 
            this.CounterVia3CrossRed.AutoSize = true;
            this.CounterVia3CrossRed.BackColor = System.Drawing.Color.Black;
            this.CounterVia3CrossRed.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CounterVia3CrossRed.ForeColor = System.Drawing.Color.Red;
            this.CounterVia3CrossRed.Location = new System.Drawing.Point(77, 242);
            this.CounterVia3CrossRed.Name = "CounterVia3CrossRed";
            this.CounterVia3CrossRed.Size = new System.Drawing.Size(30, 17);
            this.CounterVia3CrossRed.TabIndex = 7;
            this.CounterVia3CrossRed.Text = "05";
            this.CounterVia3CrossRed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CounterVia1CrossGreen
            // 
            this.CounterVia1CrossGreen.AutoSize = true;
            this.CounterVia1CrossGreen.BackColor = System.Drawing.Color.Black;
            this.CounterVia1CrossGreen.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CounterVia1CrossGreen.ForeColor = System.Drawing.Color.Green;
            this.CounterVia1CrossGreen.Location = new System.Drawing.Point(407, 142);
            this.CounterVia1CrossGreen.Name = "CounterVia1CrossGreen";
            this.CounterVia1CrossGreen.Size = new System.Drawing.Size(30, 17);
            this.CounterVia1CrossGreen.TabIndex = 8;
            this.CounterVia1CrossGreen.Text = "05";
            this.CounterVia1CrossGreen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CounterVia1TurnGreen
            // 
            this.CounterVia1TurnGreen.AutoSize = true;
            this.CounterVia1TurnGreen.BackColor = System.Drawing.Color.Black;
            this.CounterVia1TurnGreen.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CounterVia1TurnGreen.ForeColor = System.Drawing.Color.Green;
            this.CounterVia1TurnGreen.Location = new System.Drawing.Point(318, 152);
            this.CounterVia1TurnGreen.Name = "CounterVia1TurnGreen";
            this.CounterVia1TurnGreen.Size = new System.Drawing.Size(30, 17);
            this.CounterVia1TurnGreen.TabIndex = 10;
            this.CounterVia1TurnGreen.Text = "05";
            this.CounterVia1TurnGreen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CounterVia1TurnRed
            // 
            this.CounterVia1TurnRed.AutoSize = true;
            this.CounterVia1TurnRed.BackColor = System.Drawing.Color.Black;
            this.CounterVia1TurnRed.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CounterVia1TurnRed.ForeColor = System.Drawing.Color.Red;
            this.CounterVia1TurnRed.Location = new System.Drawing.Point(318, 135);
            this.CounterVia1TurnRed.Name = "CounterVia1TurnRed";
            this.CounterVia1TurnRed.Size = new System.Drawing.Size(30, 17);
            this.CounterVia1TurnRed.TabIndex = 9;
            this.CounterVia1TurnRed.Text = "05";
            this.CounterVia1TurnRed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CounterVia3CrossGreen
            // 
            this.CounterVia3CrossGreen.AutoSize = true;
            this.CounterVia3CrossGreen.BackColor = System.Drawing.Color.Black;
            this.CounterVia3CrossGreen.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CounterVia3CrossGreen.ForeColor = System.Drawing.Color.Green;
            this.CounterVia3CrossGreen.Location = new System.Drawing.Point(77, 259);
            this.CounterVia3CrossGreen.Name = "CounterVia3CrossGreen";
            this.CounterVia3CrossGreen.Size = new System.Drawing.Size(30, 17);
            this.CounterVia3CrossGreen.TabIndex = 11;
            this.CounterVia3CrossGreen.Text = "05";
            this.CounterVia3CrossGreen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CounterVia2CrossGreen
            // 
            this.CounterVia2CrossGreen.AutoSize = true;
            this.CounterVia2CrossGreen.BackColor = System.Drawing.Color.Black;
            this.CounterVia2CrossGreen.Font = new System.Drawing.Font("OCR A Extended", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CounterVia2CrossGreen.ForeColor = System.Drawing.Color.Green;
            this.CounterVia2CrossGreen.Location = new System.Drawing.Point(213, 373);
            this.CounterVia2CrossGreen.Name = "CounterVia2CrossGreen";
            this.CounterVia2CrossGreen.Size = new System.Drawing.Size(30, 17);
            this.CounterVia2CrossGreen.TabIndex = 12;
            this.CounterVia2CrossGreen.Text = "05";
            this.CounterVia2CrossGreen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(230, 409);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(78, 28);
            this.button1.TabIndex = 13;
            this.button1.Text = "Set Config";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TxtPicParamSTAT);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.TxtParamInt);
            this.groupBox1.Controls.Add(this.TxtPicParamInt);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.TxtParamCTurn);
            this.groupBox1.Controls.Add(this.TxtPicParamCTurn);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.TxtParamSTI2);
            this.groupBox1.Controls.Add(this.TxtPicParamSTI2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.TxtParamST6);
            this.groupBox1.Controls.Add(this.TxtPicParamST6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TxtParamST5);
            this.groupBox1.Controls.Add(this.TxtPicParamST5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TxtParamST4);
            this.groupBox1.Controls.Add(this.TxtPicParamST4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtParamST3);
            this.groupBox1.Controls.Add(this.TxtPicParamST3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtParamSTI1);
            this.groupBox1.Controls.Add(this.TxtPicParamSTI1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TxtParamST1);
            this.groupBox1.Controls.Add(this.TxtPicParamST1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(662, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(314, 443);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parámetros de Estados";
            // 
            // TxtPicParamSTAT
            // 
            this.TxtPicParamSTAT.Enabled = false;
            this.TxtPicParamSTAT.Location = new System.Drawing.Point(60, 24);
            this.TxtPicParamSTAT.Name = "TxtPicParamSTAT";
            this.TxtPicParamSTAT.Size = new System.Drawing.Size(55, 20);
            this.TxtPicParamSTAT.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 10);
            this.label10.TabIndex = 41;
            this.label10.Text = "STAT";
            // 
            // TxtParamInt
            // 
            this.TxtParamInt.Location = new System.Drawing.Point(121, 259);
            this.TxtParamInt.Name = "TxtParamInt";
            this.TxtParamInt.Size = new System.Drawing.Size(58, 20);
            this.TxtParamInt.TabIndex = 40;
            // 
            // TxtPicParamInt
            // 
            this.TxtPicParamInt.Enabled = false;
            this.TxtPicParamInt.Location = new System.Drawing.Point(60, 259);
            this.TxtPicParamInt.Name = "TxtPicParamInt";
            this.TxtPicParamInt.Size = new System.Drawing.Size(55, 20);
            this.TxtPicParamInt.TabIndex = 39;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 262);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 13);
            this.label9.TabIndex = 38;
            this.label9.Text = "Interm.";
            // 
            // TxtParamCTurn
            // 
            this.TxtParamCTurn.Location = new System.Drawing.Point(121, 233);
            this.TxtParamCTurn.Name = "TxtParamCTurn";
            this.TxtParamCTurn.Size = new System.Drawing.Size(58, 20);
            this.TxtParamCTurn.TabIndex = 37;
            // 
            // TxtPicParamCTurn
            // 
            this.TxtPicParamCTurn.Enabled = false;
            this.TxtPicParamCTurn.Location = new System.Drawing.Point(60, 233);
            this.TxtPicParamCTurn.Name = "TxtPicParamCTurn";
            this.TxtPicParamCTurn.Size = new System.Drawing.Size(55, 20);
            this.TxtPicParamCTurn.TabIndex = 36;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "C_TURN";
            // 
            // TxtParamSTI2
            // 
            this.TxtParamSTI2.Location = new System.Drawing.Point(121, 207);
            this.TxtParamSTI2.Name = "TxtParamSTI2";
            this.TxtParamSTI2.Size = new System.Drawing.Size(58, 20);
            this.TxtParamSTI2.TabIndex = 34;
            // 
            // TxtPicParamSTI2
            // 
            this.TxtPicParamSTI2.Enabled = false;
            this.TxtPicParamSTI2.Location = new System.Drawing.Point(60, 207);
            this.TxtPicParamSTI2.Name = "TxtPicParamSTI2";
            this.TxtPicParamSTI2.Size = new System.Drawing.Size(55, 20);
            this.TxtPicParamSTI2.TabIndex = 33;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "STI2";
            // 
            // TxtParamST6
            // 
            this.TxtParamST6.Location = new System.Drawing.Point(121, 181);
            this.TxtParamST6.Name = "TxtParamST6";
            this.TxtParamST6.Size = new System.Drawing.Size(58, 20);
            this.TxtParamST6.TabIndex = 31;
            // 
            // TxtPicParamST6
            // 
            this.TxtPicParamST6.Enabled = false;
            this.TxtPicParamST6.Location = new System.Drawing.Point(60, 181);
            this.TxtPicParamST6.Name = "TxtPicParamST6";
            this.TxtPicParamST6.Size = new System.Drawing.Size(55, 20);
            this.TxtPicParamST6.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "ST6";
            // 
            // TxtParamST5
            // 
            this.TxtParamST5.Location = new System.Drawing.Point(121, 155);
            this.TxtParamST5.Name = "TxtParamST5";
            this.TxtParamST5.Size = new System.Drawing.Size(58, 20);
            this.TxtParamST5.TabIndex = 28;
            // 
            // TxtPicParamST5
            // 
            this.TxtPicParamST5.Enabled = false;
            this.TxtPicParamST5.Location = new System.Drawing.Point(60, 155);
            this.TxtPicParamST5.Name = "TxtPicParamST5";
            this.TxtPicParamST5.Size = new System.Drawing.Size(55, 20);
            this.TxtPicParamST5.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "ST5";
            // 
            // TxtParamST4
            // 
            this.TxtParamST4.Location = new System.Drawing.Point(121, 128);
            this.TxtParamST4.Name = "TxtParamST4";
            this.TxtParamST4.Size = new System.Drawing.Size(58, 20);
            this.TxtParamST4.TabIndex = 25;
            // 
            // TxtPicParamST4
            // 
            this.TxtPicParamST4.Enabled = false;
            this.TxtPicParamST4.Location = new System.Drawing.Point(60, 128);
            this.TxtPicParamST4.Name = "TxtPicParamST4";
            this.TxtPicParamST4.Size = new System.Drawing.Size(55, 20);
            this.TxtPicParamST4.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "ST4";
            // 
            // TxtParamST3
            // 
            this.TxtParamST3.Location = new System.Drawing.Point(121, 102);
            this.TxtParamST3.Name = "TxtParamST3";
            this.TxtParamST3.Size = new System.Drawing.Size(58, 20);
            this.TxtParamST3.TabIndex = 22;
            // 
            // TxtPicParamST3
            // 
            this.TxtPicParamST3.Enabled = false;
            this.TxtPicParamST3.Location = new System.Drawing.Point(60, 102);
            this.TxtPicParamST3.Name = "TxtPicParamST3";
            this.TxtPicParamST3.Size = new System.Drawing.Size(55, 20);
            this.TxtPicParamST3.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "ST3";
            // 
            // TxtParamSTI1
            // 
            this.TxtParamSTI1.Location = new System.Drawing.Point(121, 76);
            this.TxtParamSTI1.Name = "TxtParamSTI1";
            this.TxtParamSTI1.Size = new System.Drawing.Size(58, 20);
            this.TxtParamSTI1.TabIndex = 19;
            // 
            // TxtPicParamSTI1
            // 
            this.TxtPicParamSTI1.Enabled = false;
            this.TxtPicParamSTI1.Location = new System.Drawing.Point(60, 76);
            this.TxtPicParamSTI1.Name = "TxtPicParamSTI1";
            this.TxtPicParamSTI1.Size = new System.Drawing.Size(55, 20);
            this.TxtPicParamSTI1.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "STI1";
            // 
            // TxtParamST1
            // 
            this.TxtParamST1.Location = new System.Drawing.Point(121, 50);
            this.TxtParamST1.Name = "TxtParamST1";
            this.TxtParamST1.Size = new System.Drawing.Size(58, 20);
            this.TxtParamST1.TabIndex = 16;
            // 
            // TxtPicParamST1
            // 
            this.TxtPicParamST1.Enabled = false;
            this.TxtPicParamST1.Location = new System.Drawing.Point(60, 50);
            this.TxtPicParamST1.Name = "TxtPicParamST1";
            this.TxtPicParamST1.Size = new System.Drawing.Size(55, 20);
            this.TxtPicParamST1.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "ST1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(988, 470);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CounterVia2CrossGreen);
            this.Controls.Add(this.CounterVia3CrossGreen);
            this.Controls.Add(this.CounterVia1TurnGreen);
            this.Controls.Add(this.CounterVia1TurnRed);
            this.Controls.Add(this.CounterVia1CrossGreen);
            this.Controls.Add(this.CounterVia3CrossRed);
            this.Controls.Add(this.CounterVia2CrossRed);
            this.Controls.Add(this.CounterVia1CrossRed);
            this.Controls.Add(this.SignalTurnVia1);
            this.Controls.Add(this.SignalCrossVia3);
            this.Controls.Add(this.SignalCrossVia2);
            this.Controls.Add(this.SignalCrossVia1);
            this.Controls.Add(this.StreetsImg);
            this.Name = "Form1";
            this.Text = "Control Semáforos";
            ((System.ComponentModel.ISupportInitialize)(this.SignalCrossVia3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignalCrossVia2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignalCrossVia1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StreetsImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignalTurnVia1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox StreetsImg;
        private System.Windows.Forms.PictureBox SignalCrossVia1;
        private System.Windows.Forms.PictureBox SignalCrossVia2;
        private System.Windows.Forms.PictureBox SignalCrossVia3;
        private System.Windows.Forms.PictureBox SignalTurnVia1;
        private System.Windows.Forms.Label CounterVia1CrossRed;
        private System.Windows.Forms.Label CounterVia2CrossRed;
        private System.Windows.Forms.Label CounterVia3CrossRed;
        private System.Windows.Forms.Label CounterVia1CrossGreen;
        private System.Windows.Forms.Label CounterVia1TurnGreen;
        private System.Windows.Forms.Label CounterVia1TurnRed;
        private System.Windows.Forms.Label CounterVia3CrossGreen;
        private System.Windows.Forms.Label CounterVia2CrossGreen;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TxtParamSTI2;
        private System.Windows.Forms.TextBox TxtPicParamSTI2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtParamST6;
        private System.Windows.Forms.TextBox TxtPicParamST6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtParamST5;
        private System.Windows.Forms.TextBox TxtPicParamST5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtParamST4;
        private System.Windows.Forms.TextBox TxtPicParamST4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtParamST3;
        private System.Windows.Forms.TextBox TxtPicParamST3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtParamSTI1;
        private System.Windows.Forms.TextBox TxtPicParamSTI1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtParamST1;
        private System.Windows.Forms.TextBox TxtPicParamST1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtParamInt;
        private System.Windows.Forms.TextBox TxtPicParamInt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtParamCTurn;
        private System.Windows.Forms.TextBox TxtPicParamCTurn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtPicParamSTAT;
        private System.Windows.Forms.Label label10;
    }
}

