# Electrónica Digital II - Trabajo práctico final

## __Control de funcionamiento de semáforo para intersección de calles__

### Grupo N°: 17
### Integrantes:
- Contessi, Rubén	__33315235__
- Giraudo, Juan Pablo	__34970089__


### Profesora
- Ing. Carmen Rodriguez


## Funcionamiento General

Planteamos crear un sistema de control para un conjunto de semáforos ubicados en una intersección de calles. Los mismos habilitarán y bloquearán las vías con las características luces rojas, amarillas y verdes. 

Además, contará con indicadores del tipo display de 7 segmentos que mostrarán en cuenta regresiva, los segundos restantes para un cambio de estado en cada vía.

El sistema se caracteriza por ser un sistema secuencial, por lo que contará con una cierta cantidad de estados en los que irá transitando y entregará las salidas determinada para cada uno.
En este proyecto, el control del sistema será entregado a una unidad PIC 16F887 que estará encargada de:

- Realizar la transición entre los estados del sistema
- Activar las señales de luces de los semáforos según el estado que se transite
- Contabilizar y retardar los tiempos predefinidos para cada estado
- Proporcionar conectitividad para:
  - Enviar a la central el estado del sistema en cada cambio del mismo
  - Configurar tiempos de permanencia en cada uno de los estados 
  - Configurar tiempos mostrados en displays
  - Editar parámetros que modificarán el funcionamiento del sistema

## Ubicación de las señales

Se determinará una configuración de tres vías para la intersección. 
Dos serán emplazadas en la calle principal denominados Via 1 y Via 2 y una tercera corresponderá a la calle secundaria que es de mano única, denominada Via 3.
La Vía 1 tendrá dos señalizaciones que habilitarán a los conductores a cruzar la intersección o girar hacia la izquierda por sobre la calle secundaria

Para poder especificar estados y funcionamiento, denominaremos a cada señalizacón de la siguiente forma:

- Según vías:
  - __Vía 1__
  - __Vía 2__
  - __Vía 3__
- Según naturaleza de la acción que controla:
  - __Cruce__
  - __Giro__
- Según el color de la señal:
  - __R__: Rojo
  - __A__: Amarillo
  - __V__: Verde

### Ubicación esquemática de las señales

![](./Images/DiagramaUbicación.png)


  

## Configuración de estados

Como indicamos anteriormente, un sistema de semáforos es un sistema secuencial que mediante transiciones de estado condiciona el tránsito por la intersección, evitando conflictos y accidentes entre los conductores. 

A continuación, identificaremos los estados que necesitará manejar nuestro sistema para cumplir correctamente con su función.
Y ya adentrándonos en la programación del PIC, definiremos también los nombres de los parámetros de tiempo que serán utilizados para mantener el valor deseado de permanencia en cada estado.




| Estado | Tiempo | Vía 1-Cruce | Vía 1-Giro | Vía 2-Cruce | Vía 3-Cruce |
| :----: | ------ | :----: | :----: | :----: | :----: |
| 1 | P_ST1 | ![](./Images/Semaphore%20Green.png) |  ![](./Images/TurnLeftSemaphore%20Green.png) | ![](./Images/Semaphore%20Red.png) | ![](./Images/Semaphore%20Red.png) |
| 1 | P_STI1 |![](./Images/Semaphore%20Green.png) |![](./Images/TurnLeftSemaphore%20Green.png) |![](./Images/Semaphore%20Red.png) |![](./Images/Semaphore%20Red.png) |
| 2 | P_STI1 |![](./Images/Semaphore%20Green.png) |![](./Images/TurnLeftSemaphore%20Off.png) |![](./Images/Semaphore%20Red.png) |![](./Images/Semaphore%20Red.png) |
| 3 | P_ST3 |![](./Images/Semaphore%20Green.png) |![](./Images/TurnLeftSemaphore%20Red.png) |![](./Images/Semaphore%20Green.png) |![](./Images/Semaphore%20Red.png) |
| 4 | P_ST4 |![](./Images/Semaphore%20Yellow.png) |![](./Images/TurnLeftSemaphore%20Red.png) |![](./Images/Semaphore%20Yellow.png) |![](./Images/Semaphore%20Red.png) |
| 5 | P_ST5 |![](./Images/Semaphore%20Red.png) |![](./Images/TurnLeftSemaphore%20Red.png) |![](./Images/Semaphore%20Red.png) |![](./Images/Semaphore%20Green.png) |
| 6 | P_ST6 |![](./Images/Semaphore%20Red.png) |![](./Images/TurnLeftSemaphore%20Red.png) |![](./Images/Semaphore%20Red.png) |![](./Images/Semaphore%20Yellow.png) |
| 7 | P_STI2 |![](./Images/Semaphore%20Yellow.png) |![](./Images/TurnLeftSemaphore%20Red.png) |![](./Images/Semaphore%20Yellow.png) |![](./Images/Semaphore%20Red.png) |
| 8 | P_STI2 |![](./Images/Semaphore%20Off.png) |![](./Images/TurnLeftSemaphore%20Off.png) |![](./Images/Semaphore%20Off.png) |![](./Images/Semaphore%20Off.png) |


## Detalles del sistema de control

El circuito estará controlado por un microcontrolador __PIC 16F887__ con una frecuencia de funcionamiento de 4 MHz. Al iniciar el sistema, se cargarán valores por defecto para la temporización de cada estado y se intentará conectar con la central para enviar los valores de estado del cruce.
En funcionamiento normal, la central podría enviar nuevos tiempos de retardo para cada uno de los estados. Estas modificaciones en el tiempo podrán ser permanentes, como un ajuste en el funcionamiento, o transitorias con el objetivo de sincronizar el tránsito por la arteria principal para conseguir el comportamiento denominado “onda verde”. Este permitirá a los vehículos que circulen a una velocidad de 50 km/h, alcanzar todos los cruces en verde para descongestionar la arteria.
Cuando los semáforos estén sincronizados con los de las intersecciones contiguas, la central enviará un nuevo cambio de parametros para que se coordine al funcionamiento normal. 

## Configuración de puertos para accionamiento de luces

Se utilizarán como puertos de salida para enviar las señales de encendido a cada luz de los semáforos los pines 0-7 del puerto D y 1-3 del puerto C. 
Las salidas para los estados mostrados anteriormente se definirán según la siguiente tabla:

<table>
    <thead>
        <tr>
            <th rowspan=3>
                Estado
            </th>
            <th colspan=5>
                Via 1
            </th>
            <th colspan=3>
                Via 2
            </th>
            <th colspan=3>
                Via 3
            </th>
        </tr>
        <tr>            
            <th colspan=3>
                Cruce
            </th>
            <th colspan=2>
                Giro
            </th>
            <th colspan=3>
                Cruce
            </th>
            <th colspan=3>
                Cruce
            </th>
        </tr>
        <tr>            
            <th>
                R
            </th>
            <th>
                A
            </th>
            <th>
                V
            </th>
           <th>
                R
            </th>          
            <th>
                V
            </th>
            <th>
                R
            </th>
            <th>
                A
            </th>
            <th>
                V
            </th>
            <th>
                R
            </th>
            <th>
                A
            </th>
            <th>
                V
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>1</th>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>1</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
        </tr>
         <tr>
            <th>2</th>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <th>3</th>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <th>4</th>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <th>5</th>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
        </tr>
        <tr>
            <th>6</th>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
        </tr> 
        <tr>
            <th>7</th>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <th>8</th>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
        </tr>
        <tr>
            <th>Puertos</th>
            <th>RD7</th>
            <th>RD6</th>
            <th>RD5</th>
            <th>RD4</th>
            <th>RD3</th>
            <th>RD2</th>
            <th>RD1</th>
            <th>RD0</th>
            <th>RC3</th>
            <th>RC2</th>
            <th>RC1</th>
        </tr>
    </tbody>
</table>




## Parámetros de configuración

En la tabla de [Configuracion de Estados](#configuraci%C3%B3n-de-estados) definimos una serie de parámetros que determinarán los tiempos de permanencia del sistema en cada uno de los estados. Más adelante realizaremos los cálculos para determinar cúales serán los valores por defecto de cada uno, pero además de estos parámetros, necesitaremos definir otra serie de configuraciones que nos serán útiles para determinar algunas características del funcionamiento.

En la siguiente tabla mostraremos cúales son esos parámetros, explicaremos brevemente su uso y determinaremos, en los casos que sea posible, el valor por defecto del mismo. Todos estos parámetros podrán ser editados desde la central.

| Parámetro | Uso | Valor Defecto |
| ------ | ------------------------ | ------ |
|P_C_TURN| Cantidad de veces que se repetirá la intermitencia de la luz verde en el Giro de la Vía 1  | 5 |
|P_V1CrRojDec| Valor inicial en display Rojo para Decena en el Cruce de la Vía 1 | 3 |
|P_V1CrRojUni| Valor inicial en display Rojo para Unidad en el Cruce de la Vía 1 | 4 |
|P_V1CrVerdDec| Valor inicial en display Verde para Decena en el Cruce de la Vía 1 | 5 |
|P_V1CrVerdUni| Valor inicial en display Verde para Unidad en el Cruce de la Vía 1 | 8 |
|P_V1GiRojDec| Valor inicial en display Rojo para Decena en el Giro de la Vía 1 | 6 |
|P_V1GiRojUni| Valor inicial en display Rojo para Unidad en el Giro de la Vía 1 | 9 |
|P_V1GiVerdDec| Valor inicial en display Verde para Decena en el Giro de la Vía 1 | 2 |
|P_V1GiVerdUni| Valor inicial en display Verde para Unidad en el Giro de la Vía 1 | 9 |
|P_V2CrRojDec| Valor inicial en display Rojo para Decena en el Cruce de la Vía 2 | 6 |
|P_V2CrRojUni| Valor inicial en display Rojo para Unidad en el Cruce de la Vía 2 | 4 |
|P_V2CrVerdDec| Valor inicial en display Verde para Decena en el Cruce de la Vía 2 | 2 |
|P_V2CrVerdUni| Valor inicial en display Verde para Unidad en el Cruce de la Vía 2 | 9 |
|P_V3CrRojDec| Valor inicial en display Rojo para Decena en el Cruce de la Vía 3 | 6 |
|P_V3CrRojUni| Valor inicial en display Rojo para Unidad en el Cruce de la Vía 3 | 4 |
|P_V3CrVerdDec| Valor inicial en display Verde para Decena en el Cruce de la Vía 3 | 2 |
|P_V3CrVerdUni| Valor inicial en display Verde para Unidad en el Cruce de la Vía 3 | 9 |
| Int | Bandera que determinará cúando el sistema debe entrar en intermitencia | 0 |


## Diagrama de estados 

En la imagen observaremos los estados de nuestro sistema ordenados en un diagrama de nodos, donde cada nodo representa un estado y cada flecha indica una transición entre ellos.

En los textos indicativos junto a las flechas, podemos observar las condiciones que se deberán cumplir en las variables del programa para que se produzcan las transiciones.

![](./Images/StatusDiagram.png)



## Cálculo de retardos

Para realizar los retardos necesarios entre cada cambio de estado, utilizaremos el módulo Timmer 1. Utilizando su fórmula de cálculo se determinarán los valores que se necesitan para cada uno de los tiempos de retardo necesarios.

```math
TMR1=65535-[(t/t_i)-2]/PS
```
```math
TMR1=65535-[(0.1 s/1 μs)-2]/8
```
```math
TMR1=53035
```
53035 => __CF2B__


Con este valor asignado al TMR1 obtenemos un retardo de 0.1 segundo. Como necesitamos actualizar los estados en cada segundo transcurrido, crearemos un divisor x10 para alcanzar el tiempo total necesario. Cuando este ciclo se cumpla, actualizaremos los valores de conteo de los displays, decrementaremos el tiempo del estado que está transcurriendo y enviaremos a través del puerto serie los datos actualizados del estado del proceso.
Cuando se complete la cuenta del tiempo del estado transitado, se ejecutará la modificación del estado que actualizará las variables a mostrar para el estado siguiente.

Para el refresco de las salidas y para mantener un tiempo de actualización independiente de las modificaciones de estado, utilizaremos el modulo Timer0. Deseamos obtener un retardo de 0.01 segundo para este módulo, por eso realizamos los siguientes cálculos:

```math
TMR0=256-[(t/t_i)-2]/PS
```
```math
TMR0=256-[(0.017 s/1 μs)-2]/256
```
```math
TMR0=216 
```
189 => __BD__

Al iniciar el programa, cada parámetro se inicializará con un valor por defecto. En la siguiente tabla observamos estos valores.
## Cálculo de valores para tiempos por defecto (segundos)

| Tiempo | Byte Memoria | Tiempo Defecto |
| ------ | ------ | ------ |
|P_ST1|0x50|20|
|P_STI1|0x51|1|
|P_ST3|0x52|30|
|P_ST4|0x53|5|
|P_ST5|0x54|30|
|P_ST6|0x55|5|
|P_STI2|0x56|2|

Estos valores cargados por defecto podrán ser editados desde la central mediante el puerto serie. Una vez modificados el sistema funcionará con los mismos hasta que se reinicie por alguna falla eléctrica o hasta que se vuelvan a editar.
También se podrá editar el valor de las repeticiones de la intermitencia del giro a la izquierda (P_C_TURN).

## Módulos a utilizar

- Timer 0
- Timer 1
- Módulo de comunicación Serie

## Periféricos

- Puerto A
- Puerto B
- Puerto C
- Puerto D

## Detalle de funcionamiento del programa

- Al iniciar el programa, se importa el archivo [macros.inc](https://gitlab.com/electronica-digital-ii/edii-tp-final/-/blob/master/TpFinal.X/macros.inc) en el cual definimos bloques de instrucciones que se repiten a lo largo del programa.
- Continuamos definiendo dos bloques de variables iniciadas en las pocisiones _0x20_ y _0x50_. 
  
  El primer bloque define variables utilizadas para conrolar el flujo del programa y serán seteadas a lo largo del mismo con los valores definidos en los parámetros. 
  
  El segundo define los parámetros utilizados para modificar el funcionamiento del sistema. En la sección de recepción de datos mediante el puerto serie veremos cómo estos parámetros son modificados para influir en el sistema.
- Luego definimos los saltos a los bloques de instrucciones denominados INICIALIZACION e INTERRUPCION
- __INICIALIZACION__: realiza llamadas a bloques de inicialización para cada periférico y módulo a utilizar. Al finalizar, se mantiene en un bucle infinito en el que solo limpia el registro de WATCH_DOG. Los bloques de inicialización utilizados son:
  - __I\_PUERTOS__: Inicializa los puertos utilizados como salidas y limpia los registros ANSEL y ANSELH.
  - __I\_VALORES\_DEFECTO__: Asigna a los parámetros y variables de flujo, los valores por defecto para iniciar 
  - __I\_TIMER0__: Inicialización  del Timer 0 según los cálculos realizados, el bloque tiene una definición de un sub-bloque llamado I\_TIME\_TIMER0 que es utilizado para reiniciar el conteo y es llamado luego de que la interrupcion de este timmer es resuelta.
  - __I\_TIMER1__: Inicialización  del Timer 1 según los cálculos realizados, tiene un sub-bloque similar al definido para el Timer 0 llamado I\_TIME\_TIMER1
  - __I\_PUERTO\_SERIE__: Se inicializa el puerto serie para envío y recepción de datos de forma asincrónica a una velocidad de 9600 baudios. Se habilita la recepción a la espera de datos
  - __I\_INTERRUPCIONES__: Habilitación de las siguientes interrupciones:
    - Periféricos
    - Timer 0
    - Timer 1
    - Recepción de Puerto serie
    - Deshabilito la interrupción por envío de datos del puerto serie
- __INTERRUPCION__: En este bloque se definen las llamadas a los bloques que atienden cada una de las interrupciones del sistema:
  - __INT\_TIMER1__: Ingresa cuando la interrupción la produjo el timer 1. En este bloque se produce la división x10 del tiempo para alzanzar una actualización de estados cada un segundo. Cuando se alcanza el segundo, se hace el llamado a varias funciones que tienen el siguiente formato: COUNTER\_CALC\_[Via]\_[Color]. En estas llamadas se actualizan los valores de las variables utilizadas para mostrar la cuenta en los displays.
    
    Luego se produce un decremento del tiempo del estado actual guardado en la variable TIME. En caso de finalizarse el tiempo entramos en una serie de validaciones del estado actual del sistema, este estado se encuentra guardado en la variable STAT. Al encontrar el estado actual se ingresará al método correspondiente para el mismo. En este método se definirá el cambio de estado, la actualización del tiempo para este nuevo estado y los tiempos para los indicadores display.

    Al finalizar, se setearán nuevamente la variable para el diviso x10 del tiempo. También se inicializará el timer 1 llamando a I\_TIME\_TIMER1.
    
    Cuando el tiempo transcurrido es de 1 segundo también se llama a la función INIT\_INT\_SEND\_DATA. Su funcionamiento lo explicamos más adelante.
  - __INT\_TIMER0__: Será llamado cuando la interrupción se produzca por el timer 0. Desde este método se realizarán las actualizaciones de los puertos de salida, en la llamada a SIGNALS\_REFRESH se actualizan los estados de las luces indicadoras de paso y en COUNTER\_REFRESH se hace el barrido de refresco de los valores para los displays.
  - __INT\_RECEPTION\_DATA__: Utilizado para atender la interupción generada por la recepción de datos desde el puerto serie.
  - __INT\_SEND\_DATA__: En el proceso de envío de datos cada byte enviado producirá una interrupción que será atendida por esta llamada. En la misma se enviará un nuevo byte al puerto serie.

- __INIT\_INT\_SEND\_DATA__: Esta función habilita la interrupción para el transmisor serie e inicia un contador que determinará qué posición de la palabra a enviar se estará ejecutando en esa interrupción. Luego definimos el bloque INT\_SEND\_DATA que es al cual se llama cuando ocurre la interrupción por envío de datos (TXIF). Aquí validamos qué byte de la palabra de envío insertamos en el registro TXREG. Esta selección de byte se hace con un llamado a la función BYTE\_TO\_SEND.

  La palabra que enviamos contiene tres signos menor (<) al inicio y finaliza con tres signos mayor (>). Entre estos enviamos los valores de las variables actuales del sistema y los valores de los parámetros definidos en el pic, estos pueden tener los valores definidos por defecto al inicializar o contener valores que hayan sido modificados desde el control.
- __INT\_RECEPTION\_DATA__: En este bloque se recepciona los byes que llegan al puerto serie. Un mensaje recibido contendrá un código de parámetro (ubicación en memoria del mismo) y el valor a asignarle. Al finalizar la recepción, el parámetro tendrá el valor recibido. Se espera que este mensaje inicie con un byte 0x02 y finalize con el byte 0x03.
- __SIGNALS\_REFRESH__: En este bloque se actualizan las salidas de los pines 1-3 del puerto C y el puerto D, para modificar qué señales deben estar encendidas en los semáforos según el estado del sistema.
- __COUNTER\_REFRESH__: En este bloque se realizará un barrido por todos los displays haciendo uso de los decodificadores conectados a los pines 0-3 del puerto B. Gracias a las salidas de estos pines y la habilitación de las salidas de los decodificadores con el pin RA0, se actualizará el valor de un decodificador BCD - 7 Segmentos (74HC4511). El valor al decodificador seleccionado por los demultiplexores será enviado por los pines 4-7 del puerto B.
## Mapa de carpetas

- [TpFinal.X](https://gitlab.com/electronica-digital-ii/edii-tp-final/-/tree/master/TpFinal.X) => Proyecto MPLAB
  - [program.asm](https://gitlab.com/electronica-digital-ii/edii-tp-final/-/blob/master/TpFinal.X/program.asm) => Programa de control del PIC
  - [macros.inc](https://gitlab.com/electronica-digital-ii/edii-tp-final/-/blob/master/TpFinal.X/macros.inc) => Definición de macros utilizados en el programa
- [Simulador Circuito](https://gitlab.com/electronica-digital-ii/edii-tp-final/-/tree/master/Simulador%20Circuito) => Proyecto Proteus
  - [TPFinal.pdsprj](https://gitlab.com/electronica-digital-ii/edii-tp-final/-/blob/master/Simulador%20Circuito/TPFinal.pdsprj)
- [TraficSignalsView](https://gitlab.com/electronica-digital-ii/edii-tp-final/-/tree/master/TraficSignalsView) => Proyecto WinForm desarrollado en Visual Studio
  - [/bin/Debug/TraficSignalsView.exe](https://gitlab.com/electronica-digital-ii/edii-tp-final/-/blob/master/TraficSignalsView/bin/Debug/TraficSignalsView.exe) =>  Programa ejecutable que abre el control de semáforos para PC

## Heramientas utilizadas

- MPLAB X IDE v5.35
- Proteus 8 Profesional
- Visual Studio 2019
- [Draw.IO](https://app.diagrams.net/)
- Grabación de Pantalla para Windows 10
- Excel 2016


## Video explicativo del funcionamiento

El video tiene audio explicativo que no se reproduce correctamente en esta plataforma, es necesario descargar el video para escuchar el audio correctamente.

![](./Videos/funcionamiento.mp4)